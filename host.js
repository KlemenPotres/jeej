import { getUrlPart } from './helpers/url';

export const isDev = () => {
	return process.env.NODE_ENV === 'development';
};

const defaultConfig = {
	imageURI: isDev() ? 'https://dev09.pot-chronos-1.net/files/30000/image' : 'https://dev09.pot-chronos-1.net/files/30000/image',
	graphQlURI: isDev() ? 'https://dev09.pot-chronos-1.net/graphql' : 'https://pro09.pot-chronos-1.net/graphql',
	coderegister: { // useCoderegister
		clientCrFields: {
			postal_code_id: 'aa_postal_code'
		},
		linkedCoderegisters: {
			// aa_geo_location_level2: 'aa_geo_location_level1', // ex. administrtive unit by region
		}
	}
};

export const getHostConfig = () => {
	switch (getUrlPart('hostname')) {
	default:
		return defaultConfig;
	}
};

export const demo = () => {};
