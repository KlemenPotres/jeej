import { useState } from 'react';
import { useQuery, useMutation } from 'react-query';
import { GraphQLClient } from 'graphql-request';

import { getHostConfig } from '../../host';
// import { replaceAll } from '../../helpers/common';

const useGQL = (name) => {
	const hostConfig = getHostConfig();

	const [graphQLClient] = useState(new GraphQLClient(hostConfig.graphQlURI, {
		headers: {
			'Request-Name': name,
			Apikey: '8df742e867-c11f-472a-a3298-09j5!a',
			Possessid: (process.browser && window.localStorage.getItem('possessid')) ? window.localStorage.getItem('possessid') : null
		}
	}));

	const [requestConfig, addRequestConfig] = useState({});

	const { mutateAsync } = useMutation((props) => {
		const { gqlSchema, variables } = props;

		return request(gqlSchema, variables).catch((err) => {
			console.log(err);
			const data = JSON.parse(JSON.stringify(err, undefined, 2));

			if (data && data.response) {
				const { errors } = data.response;

				// Handle potres API backend erros
				if (errors.length > 0) {
					const { error, errorData, message } = errors[0].potError;
					const { validation } = errorData;

					return { request: {}, error, message, validation };
				}
			}

			return { request: {}, error: true, message: 'Unknown server error', validation: null };
		});
	}, {
		useErrorBoundary: true
	});

	const getConfig = (type) => {
		try {
			const replaceAll = (find, replace, str) => {
				const Regex = (find === '.') ? /\./g : new RegExp(find, 'g');
				return str.replace(Regex, replace);
			};

			if (!requestConfig[name]) {
				const config = require(`../../requests/${type}/${name.indexOf('>') !== -1 ? replaceAll('>', '/', name) : name}.js`);
				addRequestConfig({ ...requestConfig, [name]: config.default });
				return config.default;
			}

			return requestConfig[name];
		} catch (err) {
			console.log('Request config load error', type, name, err);
			return false;
		}
	};

	const request = (gqlSchema, variables) => {
		if (gqlSchema !== null) {
			const data = graphQLClient.request(gqlSchema, variables || {});
			return data;
		}

		return null;
	};

	const api = (type, variables, options, headers) => {
		const config = getConfig(type);

		if (config) {
			let response = null;
			if (type === 'query') {
				response = useQuery(name, async () => request(config.gql.schema, variables)); // object is returned

				/* ex.
				const { query } = useGQL('webshop>product_list');
				const [list, setList] = useState(null);
				const { isFetched, data } = query();

				useEffect(() => {
					if (list === null && isFetched === true && data && data.request) {
						setList(data.request);
					}
				}, [isFetched]);
				*/
			} else {
				response = mutateAsync({ gqlSchema: config.gql.schema, variables }); // promise is returned

				/* ex.
				const { mutation } = useGQL('webshop>checkout');
				const submitGQL = (variables, callback = null) => {
					clearErrors();

					mutation(variables).then(({ request, validation }) => {
						if (validation) Object.keys(validation).map(key => setError(key, { message: validation[key], shouldFocus: true }));
						else if (typeof callback === 'function') callback(request);
					});
				};
				*/
			}

			return response;
		}

		return {
			status: 'error load request config',
			isError: true,
			error: `Can not load request ${name}`,
			isSuccess: false,
			isFetching: false,
			isLoading: false
		};
	};

	const query = (variables = {}, options = {}, headers = {}) => {
		return api('query', variables, options, headers);
	};

	const mutation = (variables = {}, options = {}, headers = {}) => {
		return api('mutation', variables, options, headers);
	};

	return { query, mutation, getConfig };
};

export default useGQL;
