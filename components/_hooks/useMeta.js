import { useState } from 'react';
import { useRouter } from 'next/router';

import useLanguage from './useLanguage';

const useMeta = () => {
	const [defaultMeta] = useState({
		title: 'Jeej! - zdrav polnovreden rastlinski obrok v steklenem kozarčku',
		description: 'Naroči, pripravi in uživaj, vrni ter ponovi ;)',
		keywords: '',
		image: null
	});

	// IMPORTANT: When added new, see: \drazbe\data\loader-optimistic-cache\readme.txt
	const [metaByPathname] = [{
		// pathname: lngid "id" // ex. meta_auction_list_title => meta_<id = auction_list>_title
		'/blog': 'blog_list'
	}];
	// IMPORTANT: When added new, see: \drazbe\data\loader-optimistic-cache\readme.txt

	const { t } = useLanguage();
	const router = useRouter();

	const getMetaByPathname = () => {
		const { pathname } = router;
		const m = metaByPathname[pathname] || null;

		return {
			title: m ? t(`meta_${m}_title`) : defaultMeta.title,
			description: m ? t(`meta_${m}_title`) : defaultMeta.description,
			keywords: m ? t(`meta_${m}_title`) : defaultMeta.keywords,
			image: defaultMeta.image
		};
	};

	const getMeta = (data = null) => {
		// Set meta by pathname (ex. /drazbe) - if not exist, defaultMeta is applyed
		const m = getMetaByPathname();

		// Override with data (ex. detail page)
		if (data !== null) {
			if (data.meta_title) 				m.title = data.meta_title;
			if (data.meta_description) 	m.description = data.meta_description;
			if (data.meta_keywords) 		m.keywords = data.meta_keywords;
			if (data.meta_image) 				m.image = data.meta_image;
		}

		return m;
	};

	return { getMeta, getMetaByPathname };
};

export default useMeta;
