import useApp from './useApp';

import optimisticCache from '../../data/loader-optimistic-cache/language';

const useLanguage = () => {
	const { appData } = useApp();
	const { language, isLanguageLoaded } = appData;

	const t = (key) => {
		if (isLanguageLoaded) { // loaded from local storage or server
			return language[key] || '';
		}

		return optimisticCache[key] || '';
	};

	return { isLanguageLoaded, t };
};

export default useLanguage;
