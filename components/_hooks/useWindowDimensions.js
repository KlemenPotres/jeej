import { useState, useEffect } from 'react';

const getWindowDimensions = () => {
	if (process.browser) {
		const doc = (document.compatMode === 'CSS1Compat') ? document.documentElement : document.body;
		const { clientWidth: width, clientHeight: height } = doc;

		return { width, height };
	}

	return { width: 1920, height: 1080 };
};

export default function useWindowDimensions() {
	const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

	useEffect(() => {
		const handleResize = () => {
			setWindowDimensions(getWindowDimensions());
		};

		if (process.browser) {
			window.addEventListener('resize', handleResize);
			return () => window.removeEventListener('resize', handleResize);
		}

		return false;
	}, []);

	return windowDimensions;
}
