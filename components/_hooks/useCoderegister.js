import { useState } from 'react';

import useApp from './useApp';
import useGQL from './useGQL';
import { getHostConfig } from '../../host';

const useCoderegister = () => {
	const { coderegister: crConfig } = getHostConfig();

	const { query } = useGQL();
	const { appData } = useApp();

	const getCrText = (id, name) => {
		if (appData.coderegister && appData.coderegister[name]) {
			const search = appData.coderegister[name].filter(row => row.id === id);
			if (search[0]) {
				return search[0].text;
			}
		}

		return null;
	};

	// == Get already loader, from localstore

	const getLocalData = () => {
		const storage = process.browser ? window.localStorage.getItem('loader') : {};
		const localData = JSON.parse(storage) || {};
		return localData.coderegister || {};
	};

	// Affacted field (must be filtered): parent, by which affected field is filterd
	const [linkedCoderegisters] = useState(crConfig.linkedCoderegisters);

	const [clientCrFields] = useState(crConfig.clientCrFields);

	const getCoderegister = (name) => {
		if (Object.values(appData.coderegister).length > 0) {
			const localData = appData.coderegister;

			// Return from local storage, if exists
			if (localData[name]) {
				return localData[name];
			}

			// Try to load from server, if not exist in local storage (and store in local storage)
			const serverData = loadCoderegister(name);
			if (serverData) {
				return serverData;
			}
		}

		return [];
	};

	const getCoderegisters = (crList) => {
		const collector = {};

		Object.keys(crList).map((name) => {
			if (linkedCoderegisters[name]) {
				collector[name] = crList[linkedCoderegisters[name]] ? getCoderegister(name).filter(row => row.parent_id === crList[linkedCoderegisters[name]]) : [];
				return true;
			}

			collector[name] = getCoderegister(name);
			return true;
		});

		return collector;
	};

	// == Load from server & update in local store

	const loadCoderegister = (name) => {
		if (name) {
			const request = query('coderegister>loader', { name });

			if (request.isLoading === false && request.isSuccess && request.data && request.data.request) {
				updateLocalCoderegisters(JSON.parse(request.data.request.loader), true);
			}
		}

		return false;
	};

	const updateLocalCoderegisters = (serverData, updateLocalStore) => {
		if (process.browser) {
			// Get last saved local data
			const localData = getLocalData();
			const localChecksums = localData.checksums || {};

			const d = {
				name: 'coderegister',
				checksums: { ...localChecksums, ...serverData.checksums },
				data: { ...localData.data, ...serverData.data }
			};

			if (updateLocalStore === true) {
				// Load complete local store loader and override just coderegister section
				const storage = JSON.parse(window.localStorage.getItem('loader')) || {};
				window.localStorage.setItem('loader', JSON.stringify({ ...storage, coderegister: d }));
				return true;
			}

			return d;
		}

		return serverData;
	};

	return { getCoderegister, getCoderegisters, updateLocalCoderegisters, loadCoderegister, clientCrFields, getCrText };
};

export default useCoderegister;
