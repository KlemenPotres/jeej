import { useState } from 'react';

const useLazyLoadImg = () => {
	const [loading, setLoading] = useState(false);

	const preload = async (list) => {
		setLoading(true);

		const promises = await list.map((src) => {
			return new Promise((resolve, reject) => {
				const img = new Image();

				img.src = src;
				img.onload = resolve();
				img.onerror = reject();
			});
		});

		await Promise.all(promises);
		setLoading(false);
	};

	return { loading, preload };
};

export default useLazyLoadImg;
