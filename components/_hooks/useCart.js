import { useEffect } from 'react';
import { useStore as cartStore } from '../../contexts/cart';
import useGQL from './useGQL';
import useApp from './useApp';

const useCart = (loadList = false, loadHead = false) => {
	const { appDispatch, appData: { selectSideDish }, toggleModal } = useApp();

	const { mutation: gqlCartMutate } = useGQL('webshop>cart>mutate');
	const { mutation: gqlCartRemove } = useGQL('webshop>cart>remove');
	const { mutation: gqlCartClear } = useGQL('webshop>cart>clear');

	const { data, dispatch } = cartStore();
	const { list } = data;

	if (loadHead === true) {
		const { query } = useGQL('webshop>cart>head');
		const { isSuccess, data: cartHead, refetch } = query();

		useEffect(() => {
			if (isSuccess === true && cartHead && cartHead.request) {
				dispatch({ type: 'CART_HEAD_UPDATE', payload: cartHead.request });
			}
		}, [isSuccess, cartHead]);

		// Update cart head, when cart items list is updated
		useEffect(() => {
			refetch();
		}, [list]);
	}

	if (loadList === true) {
		const { query } = useGQL('webshop>cart>list');
		const { isSuccess, data: cartItems } = query();

		useEffect(() => {
			if (isSuccess === true && cartItems && cartItems.request) {
				dispatch({ type: 'CART_UPDATE', payload: cartItems.request });
			}
		}, [isSuccess, cartItems]);
	}

	const getProductCategory = (productCodeId) => {
		return {
			category1_id: productCodeId.substring(0, 2), // food, ...
			category2_id: productCodeId.substring(2, 4) // main dish, side dish, ...
		};
	};

	const cartAdd = ({ productCodeId, quantity, quantityOriginal, isSideDish }) => {
		if (isSideDish) {
			appDispatch({ type: 'TOGGLE_SIDE_DISH_SELECTION', payload: { quantity: quantityOriginal } });
		}

		if (productCodeId) {
			gqlCartMutate({
				product_codeid: productCodeId,
				quantity
			}).then(({ request }) => {
				dispatch({
					type: 'CART_UPDATE',
					payload: request
				});

				// Detect category
				const catid = getProductCategory(productCodeId);
				if (catid.category1_id === '01') { // Food
					dispatch({ type: 'LAST_ADDED_BY_TYPE', payload: { [catid.category2_id === '02' ? 'lastMainDish' : 'lastSideDish']: productCodeId } });
				}

				if (selectSideDish) {
					appDispatch({ type: 'TOGGLE_SIDE_DISH_SELECTION', payload: { quantity: quantityOriginal } });
				}
			});
		} else if (selectSideDish) {
			appDispatch({ type: 'TOGGLE_SIDE_DISH_SELECTION', payload: { quantity: quantityOriginal } });
		}
	};

	const cartRemove = (productCodeId) => {
		gqlCartRemove({
			product_codeid: productCodeId
		}).then(({ request }) => {
			dispatch({
				type: 'CART_UPDATE',
				payload: request
			});
		});
	};

	const cartMutate = (product, quantity = 1, props = {}) => {
		const modifyQuantity = typeof props.direction !== 'undefined' ? props.direction : false;
		let q = quantity;

		if (modifyQuantity) {
			const p = list.filter(r => r.product_codeid === product.product_codeid);
			q = (p.length > 0 ? parseInt(p[0].quantity, 10) : 0) + (quantity * parseInt(modifyQuantity, 10));
		}

		if (list.length === 0 && !props.skipLocationChecker) {
			toggleModal('location-checker', {
				onClose: (d) => {
					setTimeout(() => {
						cartAdd({
							isSideDish: d.is_side_dish,
							productCodeId: d.product.product_codeid,
							quantity: d.quantity, // final quantity for current produt (old + new)
							quantityOriginal: d.quantity // only new quantity
						});
					}, 250);
				},
				product,
				quantity,
				skipLocationChecker: true,
				...props
			});
		} else {
			if (q <= 0) {
				if (product.product_codeid) {
					cartRemove(product.product_codeid);
				}
			} else {
				cartAdd({
					isSideDish: props.is_side_dish,
					productCodeId: product.product_codeid,
					quantity: q, // final quantity for current produt (old + new)
					quantityOriginal: quantity // only new quantity
				});
			}
		}
	};

	// Hide cart/checkout, if cart items list is empty
	useEffect(() => {
		if (list.length === 0) {
			dispatch({ type: 'CART_CHANGE_STEP', payload: null });
		}
	}, [list]);

	const cartClear = () => {
		gqlCartClear({}).then(({ request }) => {
			dispatch({
				type: 'CART_UPDATE',
				payload: []
			});
		});
	};

	const cartList = () => {
		return [];
	};

	const setStep = (step) => {
		dispatch({
			type: 'CART_CHANGE_STEP',
			payload: step
		});
	};

	return { data, dispatch, setStep, cartMutate, cartClear, cartList, cartRemove };
};

export default useCart;
