import { useState, useEffect } from 'react';
import { useForm as useF } from 'react-hook-form';

import useGQL from './useGQL';

const useForm = (name, initialState = {}) => {
	const { getConfig, mutation } = useGQL(name);
	const { register, handleSubmit, errors, watch, setValue, getValues, setError, clearErrors } = useF();

	const [ready, setReady] = useState(false);
	// const [config, setConfig] = useState({});
	const [fieldProps, setFieldProps] = useState({});

	useEffect(() => {
		const fields = Object.keys(initialState);
		if (fields.length > 0) {
			fields.map(key => setValue(key, initialState[key]));
		}
	}, [ready]);

	const submitGQL = (variables, callback = null) => {
		clearErrors();

		// config.request.name
		mutation(variables).then(({ request, validation }) => {
			if (validation) Object.keys(validation).map(key => setError(key, { message: validation[key], shouldFocus: true }));
			else if (typeof callback === 'function') callback(request);
		});
	};

	const updateForm = (d) => {
		const data = Object.keys(d);
		if (data.length > 0) {
			data.map(key => setValue(key, d[key]));
		}
	};

	useEffect(() => {
		if (name.length > 0) {
			const c = getConfig('mutation');
			// setConfig(c);

			if (c.form) {
				setFieldProps(c.form.fieldProps || {});
				setReady(c !== false);
			}
		}
	}, [name]);

	return { ready, handleSubmit, submitGQL, fieldProps, register, errors, watch, getValues, updateForm };
};

export default useForm;
