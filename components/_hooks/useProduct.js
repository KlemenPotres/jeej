import { getHostConfig } from '../../host';

import { useStore as productStore } from '../../contexts/product';

const useProduct = () => {
	const hostConfig = getHostConfig();
	const { data, dispatch } = productStore();
	const { list } = data;

	const productImg = (product, variation = null) => {
		return `${hostConfig.imageURI}/product/${product.product_codeid}${variation ? `-${variation}` : ''}.png?1`;
	};

	const getList = () => {
		return list;
	};

	const findProduct = (productCodeId) => {
		if (list.length) {
			const r = list.filter(row => row.product_codeid === productCodeId);
			return r[0] || {};
		}

		return {};
	};

	return { productList: list, productDispatch: dispatch, productImg, getList, findProduct };
};

export default useProduct;
