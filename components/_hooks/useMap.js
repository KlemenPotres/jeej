// import { useState } from 'react';

import useCoderegister from './useCoderegister';

const useMap = () => {
	const { getCrText } = useCoderegister();

	const loadMap = (callback) => {
		// Create the script tag, set the appropriate attributes (if not loaded before)
		if (!document.getElementById('google-map-lib')) {
			const script = document.createElement('script');
			script.src = `https://maps.googleapis.com/maps/api/js?key=${process.env.googleMapsEmbedAPIKEY}&callback=initMap`;
			script.defer = true;
			script.id = 'google-map-lib';
			document.head.appendChild(script);

			if (typeof callback === 'function') {
				window.initMap = () => callback(); // Wait for script, to be loaded - then init
			}
		} else if (typeof callback === 'function') {
			callback(); // Already loaded, just init
		}
	};

	const arePointsNear = (checkPoint, centerPoint, radiusMeters) => {
		const ky = 40000 / 360;
		const kx = Math.cos(Math.PI * centerPoint.lat / 180.0) * ky;
		const dx = Math.abs(centerPoint.lng - checkPoint.lng) * kx;
		const dy = Math.abs(centerPoint.lat - checkPoint.lat) * ky;

		return Math.sqrt(dx * dx + dy * dy) <= radiusMeters / 1000; // meteres to km
	};

	const getFullAddress = (row) => {
		return `${row.postal_address}, ${row.postal_postal_code} ${getCrText(parseInt(row.postal_postal_code, 10), 'aa_postal_code_limited')}, Slovenia`;
	};

	const geocodeAddress = (address, callback, center = { lat: 46.5563824, lng: 15.6443993 }, radius = 5000) => {
		const geocoder = new window.google.maps.Geocoder();

		geocoder.geocode({ address }, (results, status) => {
			if (status === 'OK') {
				const loc = results[0].geometry.location;
				const position = { lat: loc.lat(), lng: loc.lng() };
				const isValid = (center && radius) ? arePointsNear(center, position, radius) : null;

				if (typeof callback === 'function') {
					callback({ status: true, address, position, isValid, error: null });
				}

				return isValid;
			}

			if (typeof callback === 'function') {
				callback({ status: false, address, position: null, isValid: false, error: null });
			}

			return false;
		});
	};

	return { loadMap, geocodeAddress, arePointsNear, getFullAddress };
};

export default useMap;
