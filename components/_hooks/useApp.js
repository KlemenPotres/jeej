import { useStore as appStore } from '../../contexts/app';

const useApp = () => {
	const { data: appData, dispatch: appDispatch } = appStore();

	const toggleModal = (component, data = {}) => {
		appDispatch({
			type: 'TOGGLE_MODAL',
			payload: { component, data }
		});
	};

	return { appData, appDispatch, toggleModal };
};

export default useApp;
