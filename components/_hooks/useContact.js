// import { useState } from 'react';

import useApp from './useApp';

const useContact = () => {
	const { appDispatch } = useApp();

	const updateContact = (d) => {
		// Save in local storage for next visit
		const contactData = window.localStorage.getItem('contact') ? JSON.parse(window.localStorage.getItem('contact')) : {};
		window.localStorage.setItem('contact', JSON.stringify({ ...contactData, ...d }));

		// Update live state in app
		appDispatch({ type: 'UPDATE_CONTACT', payload: d });
	};

	return { updateContact };
};

export default useContact;
