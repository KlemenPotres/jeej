import React from 'react';
import PropTypes from 'prop-types';

import useLanguage from '../../../_hooks/useLanguage';
import useCart from '../../../_hooks/useCart';
import useApp from '../../../_hooks/useApp';
import useProduct from '../../../_hooks/useProduct';
import Carousel from '../../../carousel/carousel';
import { TimesIcon } from '../../../partials/icons';

const FoodAddon = ({ list }) => {
	const { t } = useLanguage();
	const { appData: { selectSideDish, lastQuantity }, appDispatch } = useApp();
	const { cartMutate } = useCart();
	const { productImg } = useProduct();

	return (
		<div className={`fixed z-50 bg-white bottom-0 left-0 p-8 w-11/12 rounded-xl m-auto left-0 right-0 transition-all duration-300 shadow ${selectSideDish ? 'bottom-24 h-auto' : '-bottom-full h-0 overflow-hidden'}`}>
			<div className="jeej-container">
				<button type="button" className="absolute right-6 top-6 w-47 h-47 flex justify-center items-center rounded-full shadow bg-white" onClick={() => appDispatch({ type: 'TOGGLE_SIDE_DISH_SELECTION' })}><TimesIcon /></button>
				<div className="text-xl mt-12 md:mt-0 text-center pb-8 font-bold">{t('label_select_food_addon')}</div>
				<div className="relative flex w-full">
					{list.length > 1 && (
						<Carousel options={{ responsive: { 0: { items: 1 }, 600: { items: 2 }, 1350: { items: 5 } } }} childrenCnt={list.length}>
							{list.map((row) => {
								return (
									<button key={row.product_codeid} onClick={() => cartMutate(row, lastQuantity, { direction: '+1' })} type="button" className="w-full py-3">
										<div className="relative text-center">
											<img src={productImg((row.product_codeid ? row : { product_codeid: '0000001' }), '2')} alt="" className="mx-auto" style={{ width: '150px' }} />
											<span className="border border-gray py-2 px-3 leading-3 text-sm rounded-xl">{row.product_nm_pub}{row.price > 0 ? ` (${Math.round((parseFloat(row.price) + Number.EPSILON) * 100) / 100}€)` : ''}</span>
										</div>
									</button>
								);
							})}
						</Carousel>
					)}
				</div>
			</div>
		</div>
	);
};

FoodAddon.propTypes = {
	list: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

export default FoodAddon;
