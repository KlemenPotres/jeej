import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import useLanguage from '../../../_hooks/useLanguage';

const Tabs = ({ ingredientsText, nutritionalValue, quantity }) => {
	const { t } = useLanguage();

	const [activeTab, setActiveTab] = useState(1);
	const [renderGraph, setRenderGraph] = useState(false);

	const percent = (value) => {
		return ((value.replace(',', '.')/100) * 100).toFixed(3)
	};

	useEffect(() => {
		setTimeout(() => { setRenderGraph(true); }, 150);
	}, []);

	return (
		<>
			<div className="mb-10 md:mb-6 flex justify-center md:justify-start">
				{[1, 2].map(index => (
					<button
						key={index}
						type="button"
						className={`jeej-tab focus:outline-none text-bold mr-4 text-sm ${activeTab === index ? 'active' : ''}`}
						onClick={() => setActiveTab(index)}
					>
						{t(`label_food_menu_tab${index}_title`)}
					</button>
				))}
			</div>
			{activeTab === 1 && (
				<>
					<p className="text-sm font-bold text-grayDark">{t('label_food_menu_specs_title')}:</p>
					<table className="w-full text-grayDark2 mt-4">
						<tbody>
							{nutritionalValue && Object.keys(nutritionalValue).map((name) => {
								if (name !== 'energy_value') {
									const value = nutritionalValue[name];

									return (
										<tr key={name}>
											<td className="text-sm">{t(`label_food_menu_specs_${name}`)}:</td>
											<td className="w-32 md:w-20 px-4">
												<div className="relative rounded-full h-3 w-32 md:w-20 bg-gray7 inline-block">
													<div className="absolute bg-green h-full rounded-full transition-all duration-700 w-0" style={{ minWidth: '10%', width: renderGraph ? `${percent(value)}%` : '0' }} />
												</div>
											</td>
											<td className="text-sm text-left" style={{ minWidth: '60px' }}>{renderGraph ? `${value}g` : '...'}</td>
										</tr>
									);
								}

								return false;
							})}
						</tbody>
					</table>
					<p className="text-grayDark mt-6">{t('label_food_menu_specs_energy_value')}:</p>
					<p className="text-grayDark font-bold">{nutritionalValue.energy_value}</p>
					<p className="text-grayDark mt-6">{t('label_food_menu_specs_quantity')}:</p>
					<p className="text-grayDark font-bold">{quantity}</p>
				</>
			)}

			{activeTab === 2 && <p className="text-sm text-grayDark" dangerouslySetInnerHTML={{ __html: ingredientsText }} />}
		</>
	);
};

Tabs.defaultProps = {
	ingredientsText: null
};

Tabs.propTypes = {
	ingredientsText: PropTypes.string,
	nutritionalValue: PropTypes.shape({}).isRequired
};

export default Tabs;
