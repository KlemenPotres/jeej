import React from 'react';
import PropTypes from 'prop-types';

import useProduct from '../../../_hooks/useProduct';

const Jars = ({ onClick, list, active }) => {
	const { productImg } = useProduct();

	return (
		<section className="relative z-20 pb-6 md:pb-16">
			<div className="jeej-container text-center">
				<div className="block md:flex flex-col md:flex-row items-start justify-between -mt-24 overflow-x-scroll jeej-scrollable">
					{list.map((row, index) => {
						return (
							<button key={row.product_codeid} type="button" onClick={() => onClick(index + 1)} className={`w-1/2 md:w-full px-4 py-6 lg:p-8 md:mx-2 lg:mx-4 jeej-jar cursor-pointer ${active === index + 1 ? 'active' : ''}`}>
								<img src={productImg(row, '2')} alt="" className="mx-auto" />
								<h4 className="mt-4 relative uppercase">{row.product_nm_pub}</h4>
							</button>
						);
					})}
				</div>
			</div>
		</section>
	);
};

Jars.propTypes = {
	onClick: PropTypes.func.isRequired,
	list: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
	active: PropTypes.number.isRequired
};

export default Jars;
