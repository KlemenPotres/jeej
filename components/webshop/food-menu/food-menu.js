import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import useLanguage from '../../_hooks/useLanguage';
import useGQL from '../../_hooks/useGQL';
import useCart from '../../_hooks/useCart';
import useProduct from '../../_hooks/useProduct';
import Button from '../../partials/button';
import Jars from './partials/jars';
import Tabs from './partials/tabs';
import Addon from './partials/addon';
import { ArrowRightIcon } from '../../partials/icons';

const PersonCnt = ({ personCnt, setPersonCnt }) => {
	const { t } = useLanguage();
	const [rand] = useState(Math.random());

	return (
		<div className="flex flex-col lg:mt-5 w-120">
			{[1, 2, 4].map((cnt) => {
				const id = `st-oseb-${cnt}-${rand}`;
				return (
					<div key={id} className={`rounded-full inline-block px-2 text-sm color-grayDark p-1 ${cnt === personCnt ? 'bg-gray font-bold' : ''}`}>
						<label className="flex items-center cursor-pointer" htmlFor={id}>
							<input type="radio" name="st-oseb" id={id} value={cnt} checked={cnt === personCnt} onChange={() => setPersonCnt(cnt)} className={`appearance-none outline-none w-4 h-4 border border-solid rounded-full mr-2 ${cnt === personCnt ? 'bg-radio-check bg-white border-white' : 'border-grayDark2'} bg-no-repeat bg-center`} />
							{t(`label_food_menu_person_cnt_${cnt}`)}
						</label>
					</div>
				);
			})}
		</div>
	);
};

PersonCnt.propTypes = {
	personCnt: PropTypes.number.isRequired,
	setPersonCnt: PropTypes.func.isRequired
};

const FoodMenu = () => {
	const { t } = useLanguage();

	const { query } = useGQL('webshop>product_list');
	const { isFetched, data } = query();

	const { productImg, productDispatch } = useProduct();

	const [list, setList] = useState({ mainDish: [], sideDish: [] });
	const [isList, setIsList] = useState(false);
	const [category] = useState({
		2: 'mainDish',
		3: 'sideDish'
	});

	const menuRef = useRef(null);
	const scrollIntoView = () => menuRef.current.scrollIntoView();

	useEffect(() => {
		if (isList === false && isFetched === true && data && data.request) {
			const d = data.request;
			const l = { ...list };
			l.sideDish.push({ product_codeid: null, product_nm_pub: t('label_no_side_dish'), price: 0 }); // Ne hvala. Imam.

			productDispatch({ type: 'SET_LIST', payload: d });

			d.map((row) => {
				if (row.product_category1_id === '1') { // Hrana
					l[category[parseInt(row.product_category2_id, 10)]].push(row);
				}

				return true;
			});

			setIsList(l.mainDish.length > 0);
			setList(l);
		}
	}, [isFetched]);

	const [active, setActive] = useState(1);
	const [personCnt, setPersonCnt] = useState(1);

	const toggle = (direction, position) => {
		let next = 1;

		if (!position) {
			next = active + (direction === '+' ? 1 : -1);

			if (next < 1) next = list.mainDish.length;
			if (next > list.mainDish.length) next = 1;
		} else {
			next = position;
		}

		scrollIntoView();
		setActive(next);
	};

	const { cartMutate } = useCart();

	// Slide (mobile)
	const lastX = useRef(0);
	const lastY = useRef(0);

	const touchStart = (e) => {
		lastX.current = e.touches[0].clientX;
		lastY.current = e.touches[0].clientY;
	};

	const touchEnd = (e) => {
		const te = e.changedTouches[0].clientX;
		const cY = e.changedTouches[0].clientY;

		if (lastY.current < cY + 50 && lastY.current > cY - 50) {
			if (lastX.current > te + 50) {
				toggle('+');
			} else if (lastX.current < te - 50) {
				toggle('-');
			}
		}
	};

	return (
		<>
			<section className="relative z-10 pt-16 pb-32 bg-gray6">
				<div className="jeej-container text-center" ref={menuRef}>
					<h4 className="text-4xl px-4 lg:px-0 lg:text-5xl font-bold text-orange">{t('label_food_menu_title')}</h4>
					<p className="pt-3">{t('label_food_menu_description')}</p>

					<div className="flex flex-row lg:flex-col items-start justify-between mt-2 lg:mt-16" onTouchStart={touchStart} onTouchEnd={touchEnd}>
						{isList && list.mainDish.map((row, index) => {
							const activeIndex = index + 1;
							const d = JSON.parse(row.description);
							const { links, badges } = d;

							return (
								<div key={row.product_codeid} className={`flex flex-row flex-wrap w-full lg:flex-nowrap ${activeIndex !== active ? 'hidden' : ''}`}>
									<div className="hidden lg:block w-full lg:w-1/3 mx-2">
										<div className="relative">
											<img src={productImg(row, '2')} alt="" className="mx-auto" />
											{row.price > 0 && <div className="relative text-grayDark font-bold text-2xl -mt-10 -left-4 text-right">{row.price * personCnt} €</div>}

											<div className="hidden lg:flex justify-center">
												<PersonCnt personCnt={personCnt} setPersonCnt={setPersonCnt} />
											</div>
										</div>
									</div>

									<div className="relative w-full lg:w-1/2 px-4 lg:py-6 lg:p-8 lg:mx-2 lg:mx-4 mt-4 lg:mt-0">
										<button type="button" className="absolute z-20 mt-52 right-4 md:right-24 lg:-right-4 xl:right-4 icon-box-separator-arrow w-57 h-57 flex justify-center items-center rounded-full shadow bg-white transform" onClick={() => toggle('+')}><ArrowRightIcon /></button>
										<button type="button" className="absolute z-20 mt-52 left-4 md:left-24 lg:-left-4 xl:left-4 icon-box-separator-arrow w-57 h-57 flex justify-center items-center rounded-full shadow bg-white transform rotate-180" onClick={() => toggle('-')}><ArrowRightIcon /></button>
										<h4 className="text-green my-8 lg:my-0 text-2xl font-bold leading-tight uppercase">{row.product_nm_pub} ({`${active}/${list.mainDish.length}`})</h4>
										<div className="relative flex items-start lg:static z-10">
											<img src={productImg(row, '1')} alt="" className="mx-auto top-36 left-16 w-190 md:w-300px lg:top-auto lg:left-auto lg:w-auto mt-8 relative z-20" />
											<img src={productImg(row, '2')} alt="" className="block lg:hidden mx-auto absolute top-0 left-8 md:left-40 z-10 w-250 md:w-300px" />
											{row.price > 0 && <div className="block lg:hidden absolute z-20 text-grayDark font-bold text-2xl left-4 -bottom-20">{row.price * personCnt} €</div>}
										</div>
										<button type="button" className="block mt-24 ml-2 md:mx-auto md:mt-44 lg:hidden text-white rounded-full shadow-lg bg-green py-2.5 px-10 text-xl font-bold" onClick={() => { cartMutate(row, personCnt, { is_side_dish: d.settings.is_side_dish, direction: '+1' }); }}>{t('btn_add')}</button>
										{badges && (
											<div className="flex justify-center w-full mt-10 lg:mt-6">
												{badges.vegan && <img src="/img/icons/vegan.svg" alt={t('label_vegan')} title={t('label_vegan')} className="w-8 mr-2" />}
												{badges.gluten_free && <img src="/img/icons/gluten-free.svg" alt={t('label_gluten_free')} title={t('label_gluten_free')} className="w-8" />}
											</div>
										)}
										<p className="text-grayDark text-sm mt-6 lg:min-h-desc px-12">{d.description}</p>
										<div className="hidden lg:block mt-6">
											<Button onClick={() => { cartMutate(row, personCnt, { is_side_dish: d.settings.is_side_dish, direction: '+1' }); }}>{t('btn_add')}</Button>
										</div>
									</div>

									<div className="w-full md:w-600 md:mx-auto lg:w-1/3 px-4 py-6 lg:py-8 lg:mx-2 lg:mx-4 mt-4 lg:mt-0 text-left">
										{list && activeIndex === active && <Tabs ingredientsText={d.ingredients_text} nutritionalValue={d.nutritional_value} quantity={d.quantity} />}
										<div className="mt-8">
											{links.recipe && (
												<div className="flex items-center justify-center lg:justify-start">
													<div className="w-12 mr-2">
														<img src="/img/icons/icon-tips-and-tricks.png" alt="Tips and tricks" className="mx-auto" />
													</div>
													<a href={links.recipe} target="_blank" rel="noopener noreferrer" className="underline uppercase">{t('btn_recipe')}</a>
												</div>
											)}
											{links.tipsntricks && (
												<div className="flex items-center justify-center lg:justify-start">
													<div className="w-12 mr-2">
														<img src="/img/icons/icon-recipe.png" alt="Tips and tricks" className="mx-auto" />
													</div>
													<a href={links.tipsntricks} target="_blank" rel="noopener noreferrer" className="underline uppercase">{t('btn_tips_and_tricks')}</a>
												</div>
											)}
										</div>

										<div className="flex lg:hidden items-center justify-between md:justify-center mt-12">
											<PersonCnt personCnt={personCnt} setPersonCnt={setPersonCnt} />
											<button type="button" className="text-white rounded-full shadow-lg bg-green py-2.5 px-16 md:ml-12 text-2xl font-bold" onClick={() => { cartMutate(row, personCnt, { is_side_dish: d.settings.is_side_dish, direction: '+1' }); }}>{t('btn_add')}</button>
										</div>
									</div>
								</div>
							);
						})}
					</div>
					{isList === false && <div className="text-center"><h4 className="text-lg font-bold text-orange">{t('label_loading_menu')}</h4></div>}
				</div>
			</section>
			{isList && <Jars list={list.mainDish} active={active} onClick={p => toggle(null, p)} />}
			<Addon list={list.sideDish} />
		</>
	);
};

export default FoodMenu;
