import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import useCart from '../../_hooks/useCart';
import useProduct from '../../_hooks/useProduct';
import Carousel from '../../carousel/carousel';
import { PlusIcon, MinusIcon } from '../../partials/icons';

const ProductGroup = ({ title, list, cntAll, type }) => {
	const { cartMutate, data } = useCart();

	const { productImg } = useProduct();

	const [cnt, setCnt] = useState(0);
	const [bounce, setBounce] = useState(false);
	const [activeSlide, setActiveSlide] = useState(0);

	useEffect(() => {
		if (cnt !== cntAll) {
			setCnt(list.length);
			setBounce(true);
			setTimeout(() => setBounce(false), 5000);
		}
	}, [cntAll]);

	useEffect(() => {
		const nm = type.charAt(0).toUpperCase() + type.slice(1);
		if (data[`last${nm}`]) {
			// Find index of last added and scroll to item
			const index = list.findIndex(r => r.product_codeid === data[`last${nm}`]);
			setActiveSlide(index);
		}
	}, [data.lastMainDish, data.lastSideDish]);

	return (
		<div className={`relative flex flex-wrap w-full -top-2 jeej-product-group ${bounce ? 'jeej-bounce' : ''}`}>
			{list.length > 0 && (
				<>
					<h4 className="relative md:absolute md:-top-2 flex md:opacity-0 transition-all duration-150 min-w-150 left-0 w-full md:w-2/6 justify-start pl-4 md:pl-0 md:justify-center z-50 text-sm py-1 flex md:rounded-full md:shadow md:bg-green text-green font-bold md:font-normal md:text-white">{title} ({cntAll}x)</h4>
					<Carousel options={{ responsive: { 0: { items: 1 }, 600: { items: 2 }, 1350: { items: 2 } }, remoteActiveSlide: activeSlide }} childrenCnt={list.length} controlProps={{ width: 20, height: 20, color: '#fff' }}>
						{list.map((row) => {
							return (
								<div key={row.product_codeid} className="relative flex items-center px-2 py-1.5 jeej-mini-cart">
									<div className="mr-3">
										<img src={productImg(row, '2')} alt="" className="w-16" />
										<div className="absolute z-10 md:mt-4 -left-0 bottom-2 flex jeej-quantity justify-end items-center overflow-hidden">
											<button type="button" className="w-10 h-10 min-h-10 min-w-10 md:w-5 md:h-5 md:min-h-5 md:min-w-5 focus:outline-none rounded-full bg-white mr-2 font-bold text-2xl text-gray3 shadow leading-3 mr-2 flex items-center justify-center" onClick={() => cartMutate(row, 1, { direction: '-1' })}><MinusIcon /></button>
											<button type="button" className="w-10 h-10 min-h-10 min-w-10 md:w-5 md:h-5 md:min-h-5 md:min-w-5 focus:outline-none rounded-full bg-white text-2xl text-gray3 shadow leading-3 mr-2 md:mr-0 flex items-center justify-center" onClick={() => cartMutate(row, 1, { direction: '+1' })}><PlusIcon /></button>
											<div className="rounded-full ml-2 w-5 h-5 min-w-5 min-h-5 bg-green text-white font-bold flex justify-center items-center text-sm">{row.quantity}</div>
										</div>
									</div>
									<div className="text-green jeej-product-nm max-w leading-4 text-sm font-bold pr-2 mt-6">{row.product_nm}</div>
								</div>
							);
						})}
					</Carousel>
				</>
			)}
		</div>
	);
};

ProductGroup.defaultProps = {
	title: null,
	cntAll: 0,
	type: null,
	list: []
};

ProductGroup.propTypes = {
	title: PropTypes.string,
	list: PropTypes.arrayOf(PropTypes.shape({})),
	cntAll: PropTypes.number,
	type: PropTypes.string
};

export default ProductGroup;
