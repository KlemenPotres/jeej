import React, { useState, useEffect } from 'react';

import useCart from '../_hooks/useCart';
import useLanguage from '../_hooks/useLanguage';
import Button from '../partials/button';
import Checkout from './checkout';
import ProductGroup from './partials/product-group';
import { TrashIcon, ShoppingCartIcon } from '../partials/icons';

const Cart = () => {
	const { t } = useLanguage();
	const { data: { step, list }, setStep, cartClear } = useCart(true);

	const [cartItems, setCartItems] = useState({
		'01': [], // Default
		'02': [], // Main dish
		'03': [] // Side dish
	});

	const [cntAll, setCntAll] = useState({ // by quantity
		'01': 0, // Default
		'02': 0, // Main dish
		'03': 0 // Side dish
	});

	useEffect(() => {
		if (list.length > 0) {
			const c = {
				'01': [], // Default
				'02': [], // Main dish
				'03': [] // Side dish
			};

			const cnt = { // by quantity
				'01': 0, // Default
				'02': 0, // Main dish
				'03': 0 // Side dish
			};

			list.map((row) => {
				// Get subcategory (main dish, side dish, ...) from product codeid
				const catId = row.product_codeid.substring(2, 4);

				c[catId].push(row);
				cnt[catId] += parseInt(row.quantity, 10);

				return true;
			});

			setCartItems(c);
			setCntAll(cnt);
		}
	}, [list]);

	const [cartVisible, setCartVisible] = useState(false);

	return (
		<div className={`fixed z-40 ${cartVisible ? 'bg-white shadow pt-4' : 'bg-transparent'} md:bg-white bottom-0 left-0 w-full md:shadow md:py-2 ${(['CART', 'CHECKOUT'].includes(step)) ? '' : 'hidden'}`}>
			<div className={`jeej-container flex items-center justify-between md:justify-start overflow-hidden md:overflow-visible ${cartVisible ? 'h-80 flex-col' : 'h-0'} md:h-12`}>
				{step === 'CART' && (
					<button type="button" className="absolute z-30 block md:hidden text-white rounded-full shadow-lg p-4 bg-green right-2 bottom-2 shadow" onClick={() => setCartVisible(!cartVisible)}>
						<ShoppingCartIcon color="#fff" />
						<span className="block absolute -top-1 -right-0 rounded-full w-6 h-6 min-w-6 min-h-6 bg-black text-white font-bold flex justify-center items-center text-sm">{cntAll['02'] + cntAll['03']}</span>
					</button>
				)}

				<div className="flex w-full md:w-3/4 flex-wrap md:flex md:justify-between">
					<div className="ml-4 md:ml-0 flex w-2/3 md:w-5/12">
						<ProductGroup type="mainDish" title={t('label_main_dish')} list={cartItems['02']} cntAll={cntAll['02']} />
					</div>
					<div className="ml-4 md:ml-0 flex w-2/3 md:w-5/12">
						<ProductGroup type="sideDish" title={t('label_side_dish')} list={cartItems['03']} cntAll={cntAll['03']} />
					</div>
				</div>
				{step === 'CART' && (
					<div className={`${cartVisible ? 'block' : 'hidden'} md:flex flex w-full ml-6 mb-4 md:ml-0 md:mb-0 md:w-1/2 md:w-1/4 md:justify-end relative`}>
						<Button size="sm" onClick={() => setStep('CHECKOUT')}>{t('btn_order_finish')}</Button>
						<button type="button" className="ml-2 rounded-full" onClick={() => cartClear()}><TrashIcon color="#333" /></button>
					</div>
				)}
			</div>
			<Checkout />
		</div>
	);
};

export default Cart;
