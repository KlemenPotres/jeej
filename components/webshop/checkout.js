import React, { useState, useRef, useEffect } from 'react';

import useCart from '../_hooks/useCart';
import useLanguage from '../_hooks/useLanguage';
import useForm from '../_hooks/useForm';
import useApp from '../_hooks/useApp';
import useMap from '../_hooks/useMap';
import useCoderegister from '../_hooks/useCoderegister';
import Button from '../partials/button';
import Error from '../form/error';
import { TimesIcon } from '../partials/icons';

const Checkout = () => {
	const { t } = useLanguage();
	const { loadMap, geocodeAddress, getFullAddress } = useMap();
	const { toggleModal, appData: { contact } } = useApp();
	const { data: { step, list }, setStep, cartMutate, dispatch: cartDispatch } = useCart();
	const { data: { head } } = useCart(false, true);

	const [sum, setSum] = useState({});

	const { ready, handleSubmit, submitGQL, register, errors, fieldProps, updateForm } = useForm('webshop>checkout', {
		givenname: contact.givenname || '',
		surname: contact.surname || '',
		telephone: contact.telephone || '',
		email: contact.email || '',
		postal_address: contact.postal_address || '',
		postal_postal_code: contact.postal_postal_code || ''
	});

	const { getCoderegister } = useCoderegister();
	const [postalCodes, setPostalCodes] = useState([]);

	useEffect(() => {
		updateForm({
			givenname: contact.givenname || '',
			surname: contact.surname || '',
			telephone: contact.telephone || '',
			email: contact.email || '',
			postal_address: contact.postal_address || '',
			postal_postal_code: contact.postal_postal_code || ''
		});
	}, [contact]);

	const [height, setHeight] = useState(0);
	const refCheckout = useRef(null);

	const [loading, setLoading] = useState(false);
	const [paymentTypeId, setPaymentTypeId] = useState('7001'); // 7001 = Plačilo po povzetju, 2001 = Nakazilo na trr
	const [shippingId, setShippingId] = useState('2010'); // 2010 = Naša dostava, 1010 = Osebni prevzem

	const [bail, setBail] = useState({ '0201003': false }); // kozarci + gajbica

	const onBailChange = (e) => {
		const { target } = e;
		const { value, checked } = target;

		setBail({ ...bail, [value]: checked });
		cartMutate({ product_codeid: value }, 1, { direction: !checked ? '+1' : '-1' });
	};

	useEffect(() => {
		// Check, if bail should be appended to cart (not exist on list already and not checked "i will return")
		if (step === 'CHECKOUT') {
			Object.keys(bail).map((productCodeId) => {
				if (!bail[productCodeId] && list.filter(row => row.product_codeid === productCodeId).length === 0) {
					cartMutate({ product_codeid: productCodeId }, 1, { direction: '+1' });
				}

				return true;
			});
		}

		setHeight(refCheckout.current.clientHeight);

		// Load coderegister
		setPostalCodes(getCoderegister('aa_postal_code_limited'));
	}, [step]);

	const onSubmit = (data) => {
		// Check location
		loadMap(() => {
			geocodeAddress(getFullAddress(data), (g) => {
				if (g.isValid === true) {
					setLoading(true);

					// Save to local storage
					if (process.browser) {
						window.localStorage.setItem('contact', JSON.stringify(data));
					}

					submitGQL({ ...data, payment_type_id: paymentTypeId, shipping_id: shippingId }, (d) => {
						setLoading(false);

						// Reset cart (local state)
						setStep('CART');
						cartDispatch({
							type: 'CART_UPDATE',
							payload: []
						});

						setTimeout(() => {
							toggleModal('order-issued');
							setStep(null);
						}, 500);
					});
				} else {
					setStep('CART');

					toggleModal('location-checker', {
						onClose: (d) => {
							setTimeout(() => {
								setStep('CHECKOUT');
							}, 250);
						},
						notification: { type: 'invalid', text: t('label_location_checker_invalid') }
					});
				}
			});
		});
	};

	const printDetailed = (name, field = 'grand_total') => {
		if (sum[name]) {
			return sum[name][field];
		}

		return '--';
	};

	useEffect(() => {
		setSum(head.detailed ? JSON.parse(head.detailed) : {});
	}, [head.grand_total]);

	return (
		<div className="jeej-container flex flex-wrap items-center transition-all duration-300 overflow-hidden" style={{ height: (['CHECKOUT'].includes(step) && list.length > 0) ? `${height + 80}px` : 0 }}>
			<form onSubmit={handleSubmit(onSubmit)} className="h-screen md:h-auto overflow-scroll md:overflow-visible mt-8" ref={refCheckout}>
				{step === 'CHECKOUT' && <button type="button" className="absolute right-2 top-2 w-47 h-47 flex justify-center items-center rounded-full shadow bg-white" onClick={() => setStep('CART')}><TimesIcon /></button>}
				<div className="flex flex-row flex-wrap md:flex-no-wrap">
					<div className="w-full md:w-2/3">
						<div className="border-t-2 border-solid border-lightGreen w-full pt-20 pb-6 md:py-6">
							<div className="flex flex-wrap w-full px-4 md:px-0 ">
								<div className="w-full md:w-1/6">
									<div className="mb-6 md:mb-0 text-green text-xl font-bold">{t('label_cart_payment_title')}</div>
								</div>
								<div className="md:w-5/6 flex flex-wrap">
									<label className="flex items-center w-full md:w-auto radio-label" htmlFor="pay-on-delivery">
										<input type="radio" id="pay-on-delivery" name="payment_type_id" value="7001" defaultChecked={paymentTypeId === '7001'} onChange={e => setPaymentTypeId(e.target.value)} className="w-4 h-4 mr-2" />
										{t('label_cart_payment_type1')}
									</label>
									<label className="flex items-center mt-2 md:mt-0 w-full md:w-auto md:ml-10 radio-label" htmlFor="pay-bank-transfer">
										<input type="radio" id="pay-bank-transfer" name="payment_type_id" value="2001" defaultChecked={paymentTypeId === '2001'} onChange={e => setPaymentTypeId(e.target.value)} className="w-4 h-4 mr-2" />
										{t('label_cart_payment_type2')}
									</label>
								</div>
							</div>
						</div>
						<div className="border-t-2 border-solid border-lightGreen w-full pb-4">
							<div className="mt-8 px-4 md:px-0 ">
								<div className="flex flex-wrap">
									<div className="w-full md:w-1/6">
										<div className="mb-6 md:mb-0 text-green text-xl font-bold">{t('label_cart_items_packaging')}</div>
									</div>
									<div className="w-full w-full md:w-5/6 flex">
										<label className="flex items-center checkbox-label" htmlFor="packaging-reverse">
											<input type="checkbox" id="packaging-reverse" value="0201003" checked={bail['0201003']} onChange={onBailChange} className="w-4 h-4 mr-2" />
											{t('label_cart_returning_both')}
										</label>
									</div>
								</div>
								<div className="w-full flex flex-wrap mt-3">
									<p className="text-xs" dangerouslySetInnerHTML={{ __html: t('label_cart_disclaimer') }} />
								</div>
							</div>
						</div>
						<div className="border-t-2 border-solid border-lightGreen w-full pt-10 pb-4">
							<div className="flex flex-wrap">
								<div className="w-full md:-mt-1 md:w-1/6">
									<div className="px-4 md:px-0 mb-6 md:mb-0 text-green text-xl font-bold">{t('label_cart_shipping_title')}</div>
								</div>
								<div className="w-full md:w-5/6">
									<div className="flex flex-wrap">
										<div className="px-4 md:px-0 pb-4 w-full mb-6">
											<label className="flex w-full radio-label mb-2" htmlFor="delivery-default">
												<input type="radio" id="delivery-default" name="shipping_id" value="2010" defaultChecked={shippingId === '2010'} onChange={e => setShippingId(e.target.value)} className="w-4 h-4 mr-2" />
												{t('label_cart_shipping_type1')}
											</label>
											<label className="flex w-full radio-label" htmlFor="delivery-personal">
												<input type="radio" id="delivery-personal" name="shipping_id" value="1010" defaultChecked={shippingId === '1010'} onChange={e => setShippingId(e.target.value)} className="w-4 h-4 mr-2" />
												{t('label_cart_shipping_type2')} <a href="https://goo.gl/maps/rjQKJcgDFjeN1Zha6" target="_blank" rel="noreferrer noopener" className="inline-block ml-2 underline text-green">Partizanska c. 5, 2000 Maribor</a>
											</label>
										</div>
										<div className="px-4 md:px-0 pb-4 md:pr-2 md:pb-8 w-full md:w-1/2">
											{ready && <input type="text" name="givenname" className={`border border-solid ${errors.givenname ? 'border-error' : 'border-grayDark'} rounded-full text-left px-5 py-2 w-full`} placeholder={t('label_givenname')} ref={register(fieldProps.givenname)} />}
											<Error error={errors.givenname} />
										</div>
										<div className="px-4 md:px-0 pb-4 md:pl-2 md:pb-8 w-full md:w-1/2">
											{ready && <input type="text" name="surname" className={`border border-solid ${errors.surname ? 'border-error' : 'border-grayDark'} rounded-full text-left px-5 py-2 w-full`} placeholder={t('label_surname')} ref={register(fieldProps.surname)} />}
											<Error error={errors.surname} />
										</div>
										<div className="px-4 md:px-0 pb-4 md:pr-2 md:pb-8 w-full md:w-1/3">
											{ready && <input type="text" name="telephone" className={`border border-solid ${errors.telephone ? 'border-error' : 'border-grayDark'} rounded-full text-left px-5 py-2 w-full`} placeholder={t('label_phone')} ref={register(fieldProps.telephone)} />}
											<Error error={errors.telephone} />
										</div>
										<div className="px-4 md:px-0 pb-4 md:pl-2 md:pb-8 w-full md:w-2/3">
											{ready && <input type="text" name="email" className={`border border-solid ${errors.email ? 'border-error' : 'border-grayDark'} rounded-full text-left px-5 py-2 w-full`} placeholder={t('label_mail')} ref={register(fieldProps.email)} />}
											<Error error={errors.email} />
										</div>
										<div className="px-4 md:px-0 pb-4 md:pr-2 md:pb-8 w-full md:w-2/3">
											{ready && <input type="text" name="postal_address" className={`border border-solid ${errors.postal_address ? 'border-error' : 'border-grayDark'} rounded-full text-left px-5 py-2 w-full`} placeholder={t('label_address')} ref={register(fieldProps.postal_address)} />}
											<Error error={errors.postal_address} />
										</div>
										<div className="px-4 md:px-0 pb-4 md:pl-2 md:pb-2 w-full md:w-1/3">
											{ready && (
												<select name="postal_postal_code" className={`border border-solid ${errors.postal_postal_code ? 'border-error' : 'border-grayDark'} rounded-full text-left px-5 py-2 w-full`} ref={register(fieldProps.postal_postal_code)}>
													<option>{t('label_postal_code')}</option>
													{postalCodes && postalCodes.map((row) => {
														return <option key={row.id} value={row.id}>{row.text} ({row.id})</option>;
													})}
												</select>
											)}
											<Error error={errors.postal_postal_code} />
										</div>
										<div className="px-4 md:px-0 md:pb-2 w-full md:w-full">
											{ready && <textarea name="notes" className={`border border-solid ${errors.notes ? 'border-error' : 'border-grayDark'} rounded-full px-6 py-2 w-full`} ref={register(fieldProps.notes)} placeholder={t('label_note')} />}
											<Error error={errors.notes} />
										</div>
										<div className="px-4 md:px-2 pb-4 md:pb-2 w-full md:w-full">
											<div className="">
												{ready && (
													<>
														<label className="flex items-center checkbox-label" htmlFor="gdpr_agreement">
															<input type="checkbox" id="gdpr_agreement" name="gdpr_agreement" className="w-4 h-4 mr-2" ref={register(fieldProps.gdpr_agreement)} />
															<span className="text-xs">{t('label_cart_gdrp2_text')}</span>
														</label>
														<Error error={errors.gdpr_agreement} />
													</>
												)}
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div className="w-full md:w-1/3 md:pl-20">
						<div className="border-t-2 md:border-solid md:border-2 border-lightGreen md:h-full p-6 flex justify-between flex-col">
							<div>
								<div className="text-green text-xl font-bold mb-4">{t('label_cart_items_title')}</div>

								<div className="overflow-y-auto max-h-96">
									{list && list.map((row) => {
										return (
											<p key={row.product_codeid}>{row.quantity}x {row.product_nm}<span className="ml-2">{row.price}</span></p>
										);
									})}
								</div>

								<p className="mt-4 border-t border-dashed border-lightGreen pt-4 mt-4 w-full">{`${printDetailed('main_dish', 'cnt')}x ${t('label_main_dish')}`}: <span>{printDetailed('main_dish')}</span></p>
								<p>{`${printDetailed('side_dish', 'cnt')}x ${t('label_side_dish')}`}: <span>{printDetailed('side_dish')}</span></p>
								<p>{`${printDetailed('bail', 'cnt')}x ${t('label_cart_price_bail')}`}: <span>{printDetailed('bail')}</span></p>
								<p>{t('label_cart_price_delivery')}: <span>{t('label_cart_shipping_gratis')}</span></p>
								<p className="mt-4 border-t border-dashed border-lightGreen pt-4 mt-4 w-full"><strong>{t('label_cart_price_grand_total')}: <span className="font-bold">{head.grand_total}</span></strong></p>
							</div>

							<div className="w-full mt-6">
								<Button size="sm" color="bg-black" className="w-full" onClick={() => setStep('CART')}>{t('btn_continue')}</Button>
								<Button type="submit" size="sm" className="w-full mt-2">{t(loading ? 'btn_creating_order' : 'btn_order_finish')}</Button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	);
};

export default Checkout;
