import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

// import useLanguage from '../_hooks/useLanguage';

const Error = ({ error }) => {
	// const { t } = useLanguage();
	const [message, setMessage] = useState(null);

	useEffect(() => {
		if (error) {
			const { type } = error;

			switch (type) {
			case 'required': setMessage('Obvezno polje'); break;
			case 'maxLength': setMessage('Predolg tekst'); break;
			default: setMessage(error.message); break;
			}
		} else {
			setMessage(null);
		}
	}, [error]);

	return message && <span className="text-error text-xs mt-1">{message}</span>;
};

Error.defaultProps = {
	error: null
};

Error.propTypes = {
	error: PropTypes.shape({})
};

export default Error;
