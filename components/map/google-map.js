import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import useMap from '../_hooks/useMap';

const GoogleMap = ({ center, location, onGeocoding }) => {
	const { loadMap, geocodeAddress, arePointsNear } = useMap();
	const [radius] = useState(5000);
	const [map, setMapObject] = useState(null);

	const checkAddress = (address) => {
		geocodeAddress(address, (d) => {
			const { status, position, isValid } = d;

			// Markers
			// https://sites.google.com/site/gmapsdevelopment/

			map.setCenter(d.position);
			const marker = new window.google.maps.Marker({
				map,
				position,
				icon: isValid ? 'http://maps.google.com/mapfiles/kml/pal4/icon62.png' : 'http://maps.google.com/mapfiles/kml/pal4/icon15.png',
				animation: window.google.maps.Animation.DROP
			});

			if (isValid === true) {
				if (onGeocoding) onGeocoding(d);
			} else if (onGeocoding) {
				onGeocoding({ status: false, address, position: null, isValid: false, error: `Geocode was not successful for the following reason: ${status}` });
			}
		});
	};

	useEffect(() => {
		if (location && location.length > 0) {
			checkAddress(location);
		}
	}, [location]);

	const onMapLoad = () => {
		const mapObject = new window.google.maps.Map(document.getElementById('map'), {
			center,
			zoom: 12
		});
		setMapObject(mapObject);

		// Radius circle mark
		const areaCircle = new window.google.maps.Circle({
			strokeColor: '#9BBE7B',
			strokeOpacity: 0.8,
			strokeWeight: 2,
			fillColor: '#9BBE7B',
			fillOpacity: 0.35,
			map: mapObject,
			center,
			radius, // in meteres
			clickable: false
		});

		// Center
		const marker = new window.google.maps.Marker({
			position: center,
			map: mapObject,
			animation: window.google.maps.Animation.DROP
		});
		marker.setMap(mapObject);

		window.google.maps.event.addListener(mapObject, 'click', (event) => {
			const position = { lat: event.latLng.lat(), lng: event.latLng.lng() };

			const m = new window.google.maps.Marker({
				map: mapObject,
				position,
				icon: arePointsNear(center, position, radius) ? 'http://maps.google.com/mapfiles/kml/pal4/icon62.png' : 'http://maps.google.com/mapfiles/kml/pal4/icon15.png'
			});
		});
	};

	useEffect(() => {
		loadMap(() => {
			onMapLoad();
		});
	}, []);

	return <div id="map" className="w-full h-full rounded-xl" />;
};

GoogleMap.defaultProps = {
	location: '',
	onGeocoding: null
};

GoogleMap.propTypes = {
	center: PropTypes.shape({}).isRequired,
	location: PropTypes.string,
	onGeocoding: PropTypes.func
};

export default GoogleMap;
