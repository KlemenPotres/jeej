import React, { Children } from 'react';
import PropTypes from 'prop-types';

const IconBox = ({ children, title, description, textColor, onClick, className }) => {
	return (
		<div className={`flex relative z-10`} onClick={onClick}>
			<div className="flex flex-col items-center w-240">
				<div className={`jeej-tab2 flex items-center justify-center rounded-full cursor-pointer border ${className}`}>
					{ children }
				</div>
				<div className={`font-bold mt-5 text-${textColor}`}>{ title }</div>
				{description && <p className={`text-gray-800 px-6 text-center pt-1 text-${textColor}`}>{ description }</p>}
			</div>
		</div>
	);
};

IconBox.defaultProps = {
	description: null,
	textColor: 'gray4',
	className: 'border-dashed border-gray3 bg-white bg-opacity-100 w-150 h-150',
	onClick: null
};

IconBox.propTypes = {
	children: PropTypes.node.isRequired,
	title: PropTypes.string.isRequired,
	description: PropTypes.string,
	textColor: PropTypes.string,
	onClick: PropTypes.func,
	className: PropTypes.string
};

export default IconBox;
