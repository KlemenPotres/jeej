import PropTypes from 'prop-types';
import React from 'react';
import { ArrowRightIcon } from './icons';

const Arrow = ({ className }) => {
	return (
		<div className={`icon-box-separator-arrow w-57 h-57 flex justify-center items-center rounded-full shadow bg-white transform ${className}`}>

		</div>
	);
};

Arrow.defaultProps = {
	className: '-right-12'
};

Arrow.propTypes = {
	className: PropTypes.string
};

export default Arrow;
