import React from 'react';
import PropTypes from 'prop-types';

const Banner = ({ title, description, img }) => {
	return (
		<div className="w-full lg:w-1/3 flex flex-col flex-grow flex-shrink jeej-banner">
			<div className="flex justify-center items-start flex-wrap relative md:overflow-hidden md:h-600 lg:h-auto lg:overflow-auto">
				<img src={img} alt={title} className="relative z-10 w-full" />
				<div className="absolute jeej-overlay w-full h-full top-0 left-0 z-20 bg-black opacity-30 transition-all duration-300" />
				<div className="absolute text-center text-white top-1/4 md:top-1/2 xl:top-2/4 pt-10 max-w-md mx-auto z-30">
					<h4 className="text-2xl font-bold">{ title }</h4>
					<p className="px-6 pt-4">{ description }</p>
				</div>
			</div>
		</div>
	);
};

Banner.defaultProps = {
	description: null
};

Banner.propTypes = {
	title: PropTypes.string.isRequired,
	description: PropTypes.string,
	img: PropTypes.string.isRequired
};

export default Banner;
