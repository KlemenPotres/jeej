import React from 'react';
import PropTypes from 'prop-types';
import { ArrowRightIcon } from './icons';

const IconBoxSeparator = ({ onClick, className }) => {
	return (
		<div className="icon-box-separator pt-12 relative w-full flex justify-center z-0">
			<div className="w-300% absolute top-75 border-t border-dashed border-gray4 -left-full" />
			<div className={`icon-box-separator-arrow w-57 h-57 flex justify-center items-center rounded-full shadow bg-white relative z-10 ${className || ''}`}>
				<button type="button" onClick={onClick}><ArrowRightIcon /></button>
			</div>
		</div>
	);
};

IconBoxSeparator.defaultProps = {
	onClick: null,
	className: null
};

IconBoxSeparator.propTypes = {
	onClick: PropTypes.func,
	className: PropTypes.string
};

export default IconBoxSeparator;
