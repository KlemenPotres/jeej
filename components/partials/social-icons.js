import React from 'react';
import PropTypes from 'prop-types';

import { FacebookIcon, InstagramIcon } from './icons';

const SocialIcons = ({ background, color }) => {
	return (
		<>
			<a href="https://www.facebook.com/Jeej-104383748400665" className="mr-4" target="_blank" rel="noopener noreferrer">
				<FacebookIcon background={background} color={color} width={10} height={20} />
			</a>
			<a href="https://www.instagram.com/jeej.si/" target="_blank" rel="noopener noreferrer">
				<InstagramIcon background={background} color={color} width={10} height={20} />
			</a>
		</>
	);
};

SocialIcons.defaultProps = {
	background: 'bg-lightGreen',
	color: '#9bbe7b'
};

SocialIcons.propTypes = {
	background: PropTypes.string,
	color: PropTypes.string
};

export default SocialIcons;
