import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ type, children, onClick, color, size, className }) => {
	return <button type={type} onClick={onClick} className={`text-white rounded-full shadow-lg  ${size === 'sm' ? 'py-1 px-6 h-9' : 'py-2.5 px-8 text-2xl font-bold min-w-220'} ${color} ${className}`}>{ children }</button>;
};

Button.defaultProps = {
	type: 'button',
	color: 'bg-green',
	size: 'lg',
	onClick: null,
	className: null
};

Button.propTypes = {
	type: PropTypes.string,
	children: PropTypes.string.isRequired,
	color: PropTypes.string,
	size: PropTypes.string,
	onClick: PropTypes.func,
	className: PropTypes.string
};

export default Button;
