import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import useLanguage from '../_hooks/useLanguage';
import Logo from '../partials/logo';
import NavList from './nav-list';

const Nav = ({ scrollTo }) => {
	const { t } = useLanguage();

	const [sticky, setSticky] = useState(false);

	const handleScroll = () => {
		if (window.scrollY > 10) {
			if (sticky === false) setSticky(true);
		} else if (sticky === true) {
			setSticky(false);
		}
	};

	useEffect(() => {
		window.addEventListener('scroll', handleScroll);
		return () => window.removeEventListener('scroll', handleScroll);
	});

	/*
	<div className="block md:hidden">
		<button type="button" id="nav-toggle" className="flex items-center px-3 py-2 appearance-none focus:outline-none">
			<svg className="fill-current h-5 w-5" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
				<title>Menu</title>
				<path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
			</svg>
		</button>
	</div>
	*/

	return (
		<nav id="header" className={`fixed w-full z-40 top-0 text-black bg-white ${sticky ? 'shadow' : ''}`}>
			<div className="w-full container px-4 max-w-screen-xl mx-auto flex flex-wrap items-center justify-center md:justify-between mt-0 py-3">
				<div className="flex items-center">
					<Logo />
				</div>

				<div className="w-full flex-grow md:flex md:items-center md:w-auto hidden mt-2 lg:mt-0 lg:bg-transparent text-black p-4 lg:p-0 z-20" id="nav-content">
					<NavList scrollTo={scrollTo} />
					<button type="button" id="navAction" className="bg-green text-white font-bold rounded-full py-2 px-10 uppercase" onClick={() => scrollTo('foodMenu')}>{t('btn_order')}</button>
				</div>
			</div>
		</nav>
	);
};

Nav.defaultProps = {
	scrollTo: null
};

Nav.propTypes = {
	scrollTo: PropTypes.func
};

export default Nav;
