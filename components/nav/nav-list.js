import React from 'react';
import PropTypes from 'prop-types';

import useLanguage from '../_hooks/useLanguage';

const NavList = ({ list, styles, scrollTo }) => {
	const { t } = useLanguage();

	return (
		<ul className={styles.ul || 'list-reset md:flex justify-end flex-1 items-center'}>
			{(list || [
				{ text: t('navItem1'), onClick: () => scrollTo('howItWorks'), url: null },
				// { text: t('navItem2'), onClick: () => scrollTo('local'), url: null },
				{ text: t('navItem3'), onClick: () => scrollTo('features'), url: null }
				// { text: t('navItem4'), onClick: () => scrollTo('chefs'), url: null }
			]).map((row) => {
				return (
					<li key={row.text} className={styles.li || 'mr-6 text-black'}>
						<button type="button" className="inline-block py-2 px-4" onClick={row.onClick}>{row.text}</button>
					</li>
				);
			})}
		</ul>
	);
};

NavList.defaultProps = {
	list: null,
	styles: {}
};

NavList.propTypes = {
	list: PropTypes.arrayOf(PropTypes.shape({})),
	styles: PropTypes.shape({}),
	scrollTo: PropTypes.func.isRequired
};

export default NavList;
