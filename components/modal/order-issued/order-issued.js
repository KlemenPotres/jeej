import React from 'react';

import useLanguage from '../../_hooks/useLanguage';
import SocialIcons from '../../partials/social-icons';

const OrderIssued = () => {
	const { t } = useLanguage();

	return (
		<>
			<div className="confetti">
				<div />
				<div />
				<div />
				<div />
				<div />
				<div />
				<div />
				<div />
				<div />
				<div />
				<div />
				<div />
				<div />
			</div>
			<div className="text-center">
				<img src="img/order-issued-mail.png" alt={t('label_order_issued_title')} className="inline-block" />
				<h2 className="text-2xl font-bold my-6">{t('label_order_issued_title')}</h2>
				<p dangerouslySetInnerHTML={{ __html: t('label_order_issued_text') }} />
				<div className="flex mt-6 justify-center items-center">
					<SocialIcons color="#ffffff" />
				</div>
			</div>
		</>
	);
};

export default OrderIssued;
