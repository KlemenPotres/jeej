import React from 'react';

import useApp from '../_hooks/useApp';
import OrderIssued from './order-issued/order-issued';
import LocationChecker from './location-checker/location-checker';
import { TimesIcon } from '../partials/icons';

const Modal = () => {
	const { appData: { modal: { component } }, toggleModal } = useApp();

	return (
		<div className={`fixed z-50 transition-all duration-300 ${component ? 'top-0 md:top-10 h-auto' : '-top-full h-0 overflow-hidden'} flex justify-center items-center w-full`}>
			<div className="w-full md:w-11/12 xl:w-8/12 md:rounded-xl md:shadow bg-white px-8 md:px-16 py-16 relative bg-modal h-100vh md:h-auto overflow-auto">
				<button type="button" className="absolute right-4 md:right-6 top-4 md:top-6 w-47 h-47 flex justify-center items-center rounded-full shadow bg-white" onClick={() => toggleModal(null)}><TimesIcon /></button>
				{component === 'order-issued' && <OrderIssued />}
				{component === 'location-checker' && <LocationChecker />}
			</div>
		</div>
	);
};

export default Modal;
