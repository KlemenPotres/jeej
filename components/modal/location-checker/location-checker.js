import React, { useState } from 'react';

import useLanguage from '../../_hooks/useLanguage';
import useForm from '../../_hooks/useForm';
import useApp from '../../_hooks/useApp';
import useContact from '../../_hooks/useContact';
import useCoderegister from '../../_hooks/useCoderegister';
import useMap from '../../_hooks/useMap';
import GoogleMap from '../../map/google-map';
import Button from '../../partials/button';
import Error from '../../form/error';
import { TimesIcon, CheckboxIcon } from '../../partials/icons';

const LocationChecker = () => {
	const { t } = useLanguage();
	const { getFullAddress } = useMap();
	const { appData: { modal }, toggleModal } = useApp();
	const { updateContact } = useContact();
	const { ready, register, errors, fieldProps, handleSubmit, getValues } = useForm('location_checker');

	const [notification, setNotification] = useState(modal.data.notification || null);

	const { getCoderegister } = useCoderegister();
	const [postalCodes] = useState(getCoderegister('aa_postal_code_limited'));

	const [isValid, setIsValid] = useState(null);

	const onGeocoding = (data) => {
		const type = data.isValid ? 'valid' : 'invalid';
		setIsValid(data.isValid);
		setNotification({ type, text: t(`label_location_checker_${type}`) });

		// Update in local storage, where user data is saved
		if (type === 'valid' && process.browser) {
			updateContact(getValues());
		}
	};

	const [location, setLocation] = useState('');

	const onSubmit = (data) => {
		if (isValid) {
			toggleModal(null);
		} else {
			setLocation(getFullAddress(data));
		}
	};

	return (
		<div className="text-center">
			<h2 className="text-2xl font-bold">{t('label_location_checker_title')}</h2>
			<p dangerouslySetInnerHTML={{ __html: t('label_location_checker_text') }} className="mt-6" />
			<div className="w-full relative z-10 h-48 md:h-80 m-auto mt-6">
				<GoogleMap center={{ lat: 46.5563824, lng: 15.6443993 }} location={location} onGeocoding={onGeocoding} />
			</div>
			{notification && (
				<p className={`text-white flex items-center justify-center w-full p-2 ${notification.type === 'invalid' ? 'text-error' : 'text-green'} mt-6`}>
					<div className={`rounded-full flex items-center justify-center w-5 min-w-5 min-h-5 h-5 mr-2 ${notification.type === 'invalid' ? 'bg-error' : 'bg-green'}`}>{notification.type === 'invalid' ? <TimesIcon color="#fff" width="12" height="12" /> : <CheckboxIcon color="#fff" width="12" height="12" />}</div>
					<span>{notification.text}</span>
				</p>
			)}
			<form onSubmit={handleSubmit(onSubmit)} className="flex flex-wrap mt-6 items-center text-left">
				<div className="md:px-2 md:mt-0 w-full md:w-1/3 lg:w-2/4">
					{ready && <input type="text" name="postal_address" className={`border border-solid ${errors.postal_address ? 'border-error' : 'border-grayDark'} rounded-full text-left px-5 h-11 w-full`} placeholder={t('label_address')} ref={register(fieldProps.postal_address)} />}
					<Error error={errors.postal_address} />
				</div>
				<div className="md:px-2 mt-6 md:mt-0 w-full md:w-1/3 lg:w-1/4">
					{ready && (
						<select name="postal_postal_code" defaultValue="2000" className={`border border-solid ${errors.postal_postal_code ? 'border-error' : 'border-grayDark'} rounded-full text-left px-5 h-11 w-full`} ref={register(fieldProps.postal_postal_code)}>
							<option>{t('label_postal_code')}</option>
							{postalCodes && postalCodes.map((row) => {
								return <option key={row.id} value={row.id}>{row.text} ({row.id})</option>;
							})}
						</select>
					)}
					<Error error={errors.postal_postal_code} />
				</div>
				<div className="md:px-2 w-full mt-6 md:mt-0 md:w-1/3 lg:w-1/4 flex">
					<Button type="submit" size="sm" className="h-11 w-full min-w-200px" color={isValid ? 'bg-green' : 'bg-black'}>{t(isValid ? 'btn_continue' : 'btn_check')}</Button>
				</div>
			</form>
		</div>
	);
};

export default LocationChecker;
