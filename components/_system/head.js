import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import PropTypes from 'prop-types';

import useMeta from '../_hooks/useMeta';

const DocumentHead = ({ data }) => {
	const { getMeta } = useMeta();
	const [meta, setMeta] = useState(getMeta(null));

	useEffect(() => {
		if (data) setMeta(getMeta(data || null));
	}, [data]);

	return (
		<Head>
			<title>{meta.title}</title>
			<meta name="title" content={meta.title} />
			<meta name="description" content={meta.description} />
			<meta name="keywords" content={meta.keywords} />
			<meta property="og:title" content={meta.title} />
			<meta property="og:description" content={meta.description} />
			{meta.image && <meta property="og:image" content={meta.image} />}
			{process.browser && <meta property="og:url" content={window.top.location.href} />}
			<meta name="twitter:card" content="summary_large_image" />
			<link rel="icon" href="/favicon.ico" />
			<link rel="manifest" href="/manifest.webmanifest" />
			<meta name="theme-color" content="#6872E3" />
			<meta name="apple-mobile-web-app-status-bar-style" content="black" />
			<meta name="apple-mobile-web-app-title" content="Mojedrazbe.com" />
			<meta name="apple-mobile-web-app-capable" content="yes" />
			<meta name="mobile-web-app-capable" content="yes" />
			<link rel="icon" href="/icons/48x48.png" />
			<link rel="apple-touch-icon" sizes="48x48" href="/icons/48x48.png" />
			<link rel="apple-touch-icon" sizes="72x72" href="/icons/72x72.png" />
			<link rel="apple-touch-icon" sizes="96x96" href="/icons/96x96.png" />
			<link rel="apple-touch-icon" sizes="144x144" href="/icons/144x144.png" />
			<link rel="apple-touch-icon" sizes="192x192" href="/icons/192x192.png" />
			<link rel="apple-touch-icon" sizes="256x256" href="/icons/256x256.png" />
			<link rel="apple-touch-icon" sizes="384x384" href="/icons/384x384.png" />
			<link rel="apple-touch-icon" sizes="512x512" href="/icons/512x512.png" />
			<link rel="preconnect" href="https://fonts.gstatic.com/" crossOrigin />
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" />
		</Head>
	);
};

DocumentHead.defaultProps = {
	data: {}
};

DocumentHead.propTypes = {
	data: PropTypes.shape({})
};

export default DocumentHead;
