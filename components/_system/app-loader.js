import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import useGQL from '../_hooks/useGQL';
import useApp from '../_hooks/useApp';
// import useSession from '../_hooks/useSession';
import useCoderegister from '../_hooks/useCoderegister';
import { isEmptyObject } from '../../helpers/common';

const AppLoader = ({ children, onLoad, load }) => {
	const { query } = useGQL('loader');
	const { query: querySessionId } = useGQL('session>generateid');

	const { appDispatch } = useApp();
	const { updateLocalCoderegisters } = useCoderegister();

	const [isSSR] = useState(!process.browser);
	const storage = isSSR === false ? window.localStorage.getItem('loader') : null;
	const localData = storage ? JSON.parse(storage) : {};

	// Create possess id and save it to localstorage, if not exist yet
	if (process.browser && !window.localStorage.getItem('possessid')) {
		const sessionRequest = querySessionId();

		useEffect(() => {
			if (sessionRequest.isSuccess === true && sessionRequest.data && sessionRequest.data.request) {
				window.localStorage.setItem('possessid', sessionRequest.data.request.possessid);
			}
		}, [sessionRequest]);
	}

	// Set loaders
	const loaders = [
		{
			name: 'properties',
			enabled: load.properties || false
		},
		{
			name: 'config',
			enabled: load.config || false
		},
		{
			name: 'language',
			enabled: load.language || false
		},
		{
			name: 'coderegister',
			enabled: load.coderegister || false
		}
	];

	// Get all last modification timestampes for each section, which is already in cache
	const modified = {};
	loaders.map((row) => {
		if (row.enabled && localData[row.name]) {
			// Config & languages
			if (localData[row.name].last_modified) {
				modified[row.name] = localData[row.name].last_modified;
			}

			// Coderegisters
			if (localData[row.name].checksums) {
				modified[row.name] = localData[row.name].checksums;
			}
		}
		return true;
	});

	const [fetchedData, setFetchedData] = useState(null);
	const { isFetched, data } = query({
		// Last modified records in local storage, to optimize load
		modified: JSON.stringify(modified),

		// Define, what to load
		properties: load.properties || false,
		config: load.config || false,
		language: load.language || false,
		coderegister: load.coderegister || false
	});

	// Local store fetched data
	useEffect(() => {
		if (fetchedData === null && isFetched === true && data && data.request) {
			setFetchedData(data.request);
		}
	}, [isFetched]);

	// Handle fetched data
	useEffect(() => {
		if (fetchedData !== null) {
			loaders.map((row) => {
				if (row.enabled) {
					if (row.name === 'properties') {
						if (isSSR === false && fetchedData.properties) {
							window.localStorage.setItem('properties', fetchedData.properties);
							appDispatch({ type: 'SET_PROPERTIES', payload: JSON.parse(fetchedData.properties) });
						}
					} else {
						// Convert string to object
						fetchedData[row.name] = typeof fetchedData[row.name] !== 'object' ? JSON.parse(fetchedData[row.name]) : fetchedData[row.name];

						// Continue only, if changed and not empty (client) OR if SSR
						if (isSSR === true || (fetchedData[row.name].changed && !isEmptyObject(fetchedData[row.name]))) {
							if (!localData[row.name]) {
								localData[row.name] = {};
							}

							// Load data from server & cache in local storage
							switch (row.name) {
							default: {
								localData[row.name] = {
									name: row.name,
									last_modified: fetchedData[row.name].last_modified,
									data: fetchedData[row.name].data
								};

								break;
							}

							case 'coderegister': {
								if (fetchedData[row.name].data) {
									localData[row.name] = updateLocalCoderegisters(fetchedData[row.name], false);
								}

								break;
							}
							}
						}
					}
				}

				return true;
			});

			// Save last settings & data to local storage
			if (localData) {
				// Save to local storage (if not SSR) - so client can use cached version
				if (isSSR === false) {
					window.localStorage.setItem('loader', JSON.stringify(localData));
				}

				Object.keys(localData).map((name) => {
					if (load[name] === true) {
						return appDispatch({
							type: 'SET_LOADER_DATA',
							payload: {
								name,
								data: localData[name].data || {}
							}
						});
					}

					return true;
				});

				onLoad(true);
			}
		}
	}, [fetchedData]);

	useEffect(() => {
		// Initial load with cached data - if exist - then request is sent to server and update, if any changeds in config, language and/or coderegisters
		if (localData && (localData.config && localData.config.data) && (localData.language && localData.language.data) && (localData.coderegister && localData.coderegister.data)) {
			Object.keys(localData).map((name) => {
				return appDispatch({
					type: 'SET_LOADER_DATA',
					payload: {
						name,
						data: localData[name].data || {}
					}
				});
			});
		}
	}, []);

	return null;
};

AppLoader.defaultProps = {
	children: null,
	load: {}
};

AppLoader.propTypes = {
	children: PropTypes.node,
	onLoad: PropTypes.func.isRequired,
	load: PropTypes.shape({})
};

export default AppLoader;
