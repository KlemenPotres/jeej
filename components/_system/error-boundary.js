import { Component } from 'react';
import PropTypes from 'prop-types';

class ErrorBoundary extends Component {
	constructor(props) {
		super(props);
		this.state = { hasError: false, error: null };
	}

	static getDerivedStateFromError(error) {
		// Update state so the next render will show the fallback UI.
		return { hasError: true, error };
	}

	componentDidCatch(error, errorInfo) {
		// You can also log the error to an error reporting service
		// logErrorToMyService(error, errorInfo);
		console.log('ERROR BOUNDARY', error, errorInfo);
	}

	render() {
		// const { hasError } = this.state;
		const { children } = this.props;

		// if (hasError) return <h1>Something went wrong.</h1>;
		return children;
	}
}

ErrorBoundary.propTypes = {
	children: PropTypes.element.isRequired
};

export default ErrorBoundary;
