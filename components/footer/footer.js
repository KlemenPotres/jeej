import React from 'react';
import PropTypes from 'prop-types';

import useLanguage from '../_hooks/useLanguage';
import Logo from '../partials/logo';
import NavList from '../nav/nav-list';
import SocialIcons from '../partials/social-icons';

const Footer = ({ scrollTo }) => {
	const { t } = useLanguage();

	return (
		<footer className="bg-footer bg-green pt-40 pb-6 px-12  text-white margin ">
			<div className="container mx-auto">
				<div className="flex justify-center">
					<Logo color="#dcf21a" />
				</div>
				<NavList styles={{ ul: 'list-reset p-8 flex justify-center flex-wrap', li: 'inline-block mx-6 md:block' }} scrollTo={scrollTo} />
				<div className="flex justify-center items-center pb-10">
					<span className="mr-10">{t('label_follow_us')}:</span>
					<div className="flex justify-center items-center">
						<SocialIcons background="bg-yellow" />
					</div>
				</div>
				<p className="w-full text-center pb-20">{t('label_contact')} <br /> <a className="inline-block mt-2 hover:text-yellow" href="mailto:zdravo@jeej.si">zdravo@jeej.si</a></p>
				<p className="text-center text-xs">&copy; {t('label_copyrights')}</p>
				<p className="text-center text-xs mt-2"><i>Spletna stran je konceptualne narave in oddana naročila ne bodo obdelana.</i></p>
			</div>
		</footer>
	);
};

Footer.propTypes = {
	scrollTo: PropTypes.func.isRequired
};

export default Footer;
