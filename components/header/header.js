import React from 'react';
import PropTypes from 'prop-types';

import useLanguage from '../_hooks/useLanguage';
import Button from '../partials/button';

const Header = ({ onClick, buttonText }) => {
	const { t } = useLanguage();

	return (
		<header className="mb-44 md:mb-32">
			<div className="absolute top-0 left-0 w-full h-hero-bg md:min-h-hero-bg" />
			<div className="relative bg-hero-mobile md:bg-hero md:min-h-hero bg-white bg-no-repeat bg-center bg-top bg-cover relative">
				<div className="relative z-10 text-white text-left z-1 max-w-screen-xl flex mx-auto flex-col justify-center h-full pb-36">
					<h1 className="text-6xl mt-36 md:text-8xl font-bold text-black text-shadow-hero mb-2 md:mb-8 ml-4 md:ml-8">{t('label_hero_title')}</h1>
					<div className="p-6 md:p-0 md:ml-24 block w-full md:w-5/12">
						<p className="py-2 inline mb-2 text-lg"><span className="inline-text-background bg-greenLight font-medium text-black">{t('label_hero_description_line1') }</span></p><br /><br />
						<p className="py-2 inline text-lg"><span className="inline-text-background bg-greenLight font-medium text-black">{t('label_hero_description_line2') }</span></p>
					</div>
				</div>
				<div className="absolute -bottom-10 md:right-0 md:-bottom-40 flex justify-center w-full">
					<img src="https://dev09.pot-chronos-1.net/files/30000/image/product/0102005-2.png?1" className="relative -ml-2 md:ml-36 2xl:ml-200 xl:ml-96 w-32 top-10 md:top-0 md:w-52 lg:w-72" alt="food jar" />
					<img src="https://dev09.pot-chronos-1.net/files/30000/image/product/0102002-2.png?1" className="relative md:w-52 lg:w-72 w-32 top-2 md:-top-12 2xl:-top-20" alt="food jar" />
					<img src="https://dev09.pot-chronos-1.net/files/30000/image/product/0102009-2.png?1" className="relative md:w-52 lg:w-72 w-28 -top-8 md:-top-24 2xl:-top-48" alt="food jar" />
				</div>
				{onClick && buttonText && <span className="absolute block m-auto md:hidden -bottom-40 justify-center flex w-full"><Button onClick={onClick}>{buttonText}</Button></span>}
			</div>
		</header>
	);
};

Header.defaultProps = {
	onClick: null,
	buttonText: null
};

Header.propTypes = {
	onClick: PropTypes.func,
	buttonText: PropTypes.string
};

export default Header;
