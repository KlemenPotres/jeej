import React from 'react';

import useLanguage from '../_hooks/useLanguage';

const Chefs = () => {
	const { t } = useLanguage();

	return (
		<>
			<section className="hidden md:block min-h-maja-nina bg-gray5 bg-nina-maja bg-no-repeat bg-center bg-top">
				<div className="jeej-container relative">
					<div className="max-w-xs p-5 inline-block absolute 2xl:-right-44 2xl:top-16 lg:-right-10 lg:top-72 text-white">
						<h3 className="text-6xl">{t('label_chef_1_title')}</h3>
						<p className="text-sm pt-3 pl-8">{t('label_chef_1_description')}</p>
					</div>
					<div className="max-w-xs p-5 inline-block absolute top-16 left-0 md:left-80">
						<h3 className="text-6xl">{t('label_chef_2_title')}</h3>
						<p className="text-sm pt-3 pl-8">{t('label_chef_2_description')}</p>
					</div>
				</div>
			</section>

			<section className="block md:hidden min-h-maja-nina-sm bg-nina-maja bg-no-repeat" style={{ backgroundPosition: '-970px 0px' }}>
				<div className="jeej-container relative">
					<div className="max-w-xs p-5 inline-block mt-72 mt-52 bg-white bg-opacity-60 shadow">
						<h3 className="text-6xl">{t('label_chef_1_title')}</h3>
						<p className="text-sm pt-3 pl-8">{t('label_chef_1_description')}</p>
					</div>
				</div>
			</section>

			<section className="block md:hidden min-h-maja-nina-sm bg-nina-maja bg-no-repeat" style={{ backgroundPosition: '-100px -100px' }}>
				<div className="block md:hidden jeej-container relative">
					<div className="max-w-xs p-5 inline-block absolute ml-24 mt-52 bg-white bg-opacity-60 shadow">
						<h3 className="text-6xl">{t('label_chef_2_title')}</h3>
						<p className="text-sm pt-3 pl-8">{t('label_chef_2_description')}</p>
					</div>
				</div>
			</section>
		</>
	);
};

export default Chefs;
