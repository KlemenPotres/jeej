import React, { useState } from 'react';
import PropTypes from 'prop-types';

import useLanguage from '../_hooks/useLanguage';
import Button from '../partials/button';
import IconBox from '../partials/icon-box';
import IconBoxSeparator from '../partials/icon-box-separator';

const Process = ({ name, title, text, list, onClick, buttonText, styles }) => {
	const { t } = useLanguage();
	const [listLength] = useState(list.length);

	return (
		<>
			<section className={`${styles.header || 'pt-12 pb-6 md:pb-12'}`}>
				<div className="jeej-container-narrow text-center md:px-12">
					<h4 className={`${styles.headerTitle || 'text-2xl font-bold'}`}>{title}</h4>
					<p className="pt-3 px-6">{text}</p>
				</div>
			</section>

			<section className={`${styles.body || ''}`}>
				<div className="w-full p-6 flex justify-start xl:justify-center flex-grow flex-shrink max-w-screen-2xl mx-auto overflow-scroll md:overflow-auto">
					{list.map((icon, i) => {
						const stringIndex = i + 1;
						return (
							<React.Fragment key={stringIndex}>
								<IconBox title={t(`label_process_${name}_step${stringIndex}_title`)} description={t(`label_process_${name}_step${stringIndex}_description`)}>{icon}</IconBox>
								{listLength > i + 1 && <IconBoxSeparator className="invisible md:visible" />}
							</React.Fragment>
						);
					})}
				</div>

				{buttonText && (
					<div className="text-center mt-8 md:mt-0 pt-6 md:pb-6">
						<Button onClick={onClick}>{buttonText}</Button>
					</div>
				)}
			</section>
		</>
	);
};

Process.defaultProps = {
	title: null,
	text: null,
	styles: {},
	onClick: null,
	buttonText: null
};

Process.propTypes = {
	name: PropTypes.string.isRequired,
	title: PropTypes.string,
	text: PropTypes.string,
	list: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
	styles: PropTypes.shape({}),
	onClick: PropTypes.func,
	buttonText: PropTypes.string
};

export default Process;
