import React, { useState, useEffect, useRef } from 'react';

import useLanguage from '../_hooks/useLanguage';
import useLazyLoadImg from '../_hooks/useLazyLoadImg';
import useOnScreen from '../_hooks/useOnScreen';

const Features = () => {
	const { t } = useLanguage();
	const { preload } = useLazyLoadImg();

	// Preload images, when section on screen
	const ref = useRef();
	const onScreen = useOnScreen(ref);

	const [activeTab, setActiveTab] = useState(0);
	const [isPreloaded, setIsPreloaded] = useState(false);
	const [images] = useState({
		0: '/img/filozofija_lokalni_dobavitelji.jpg',
		1: '/img/filozofija_nabava.jpg',
		2: '/img/filozofija_kuhanje_brez_e_jev.jpg',
		3: '/img/filozofija_povratna_embalaza.jpg'
	});
	const [list] = useState(['/img/ilustracija_lokalni_dobavitelji.svg', '/img/ilustracija_nabava.svg', '/img/ilustracija_kuhanje_brez_ejev.svg', '/img/ilustracija_povratna_embalaza.svg']);

	useEffect(() => {
		if (isPreloaded === false && onScreen) {
			preload(Object.values(images)); // background images
			preload(list); // icons
			
			setIsPreloaded(true);
		}
	}, [onScreen]);

	const [position] = useState({
		0: 'md:top-0 md:left-1/2 md:transform md:-translate-x-1/2 md:-translate-y-1/2',
		1: 'md:top-1/2 md:right-0 md:transform md:translate-x-1/2 md:-translate-y-1/2',
		2: 'md:bottom-0 md:left-1/2 md:transform md:-translate-x-1/2 md:translate-y-1/2',
		3: 'md:top-1/2 md:left-0 md:transform md:-translate-x-1/2 md:-translate-y-1/2'
	});

	return (
		<section className="pt-0 pb-0 md:pt-8 md:pb-16 relative bg-no-repeat bg-cover bg-center-center md:bg-center md:bg-top md:min-h-900" style={{ backgroundImage: `url('${images[activeTab]}')` }} ref={ref}>
			<div className="relative md:absolute md:left-1/2 md:top-1/2 md:w-600 md:h-250 md:h-600 md:rounded-full md:border-18 md:border-white md:bg-white md:bg-opacity-80 md:transform md:-translate-x-1/2 md:-translate-y-1/2 md:shadow">
				<div className="md:hidden absolute w-full h-full top-0 left-0 z-0 bg-black opacity-25" />
				<div className="relative z-10 w-full flex md:p-6 max-w-screen-2xl mx-auto md:h-600 overflow-scroll md:overflow-visible">
					{list.map((icon, index) => {
						const stringIndex = index + 1;

						return (
							<button
								key={stringIndex}
								type="button"
								className={`jeej-feature flex my-6 mx-6 md:mx-0 md:my-0 relative md:absolute cursor-pointer rounded-full bg-white bg-opacity-80 min-w-150 w-150 min-h-150 w-150 z-10 flex items-center justify-center p-4 text-center font-bold shadow ${position[index]} ${activeTab === index ? 'active' : ''}`}
								onClick={() => setActiveTab(index)}
							>
								{t(`label_features${stringIndex}_title`)}
							</button>
						);
					})}
				</div>

				{list.map((icon, index) => {
					const stringIndex = index + 1;

					return activeTab === index && (
						<div key={stringIndex} className="relative md:absolute w-full z-0 top-0 h-full flex flex-col text-white md:text-black justify-center items-center text-center p-6 md:p-24">
							<span className="hidden md:block w-full"><img src={icon} className="w-full" alt={t(`label_features${stringIndex}_title`)} /></span>
							<p className="md:mt-2 min-h-12">{t(`label_features${index + 1}_description`)}</p>
						</div>
					);
				})}

			</div>
		</section>
	);
};

export default Features;
