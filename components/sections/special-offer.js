import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router'

import useLanguage from '../_hooks/useLanguage';
import useProduct from '../_hooks/useProduct';
import useCart from '../_hooks/useCart';
import { TwoPersonsIcon } from '../partials/icons';
import Button from '../partials/button';

const SpecialOffer = ({ scrollTo }) => {
	const router = useRouter();
	const { query } = router;

	const { t } = useLanguage();
	const { productList, productImg, findProduct } = useProduct();
	const { cartMutate } = useCart();

	const [ready, setReady] = useState(false);
	const [product, setProduct] = useState({	});

	// https://docs.google.com/spreadsheets/d/1Mov7uixrcLuo-QZvhJs_RdUbzWfaF3fc9Kxf03gjNK0/edit#gid=0
	const [combo] = useState([
		['0102006', '0103003'], // Čili brezgrešni + Polenta
		['0102007'], // Slovenska žurka v solati
		['0102008', '0103002'], // Lečina čorba + Riž
		['0102009'], // Solatni Orient ekspres
		['0102010'] // Segedin po naše
	]);

	useEffect(() => {
		if (productList.length > 0) {
			setReady(true);
			setProduct(findProduct('0201005'));
		}
	}, [productList]);

	// if (!query.skupaj) return false;

	return ready && (
		<section className="relative md:min-h-special-offer bg-green md:bg-cover md:bg-special-offer">
			<div className="hidden md:block absolute w-full h-full z-0 bg-black bg-opacity-50" />
			<div className="relative z-10 text-white text-center max-w-screen-xl flex mx-auto flex-col justify-center h-full py-16">
				<h1 className="relative px-6 md:px-0 text-6xl md:text-8xl font-bold flex w-full justify-center">
					{t('label_special_offer_crate_title')}
					<div className="hidden absolute md:relative -top-4 right-14 md:right-5 rounded-full w-16 h-16 flex justify-center items-center border-2 border-white bg-black text-xl">{Math.round(product.price, 2)} <span className="pl-1 text-sm">€</span></div>
				</h1>
				<p className="pt-10 px-4 w-full" dangerouslySetInnerHTML={{ __html: t('label_special_offer_crate_description') }} />
				<>
					<div className="relative w-full md:justify-between pb-10 md:pb-0 overflow-auto -top-4 md:top-0">
						<div className="w-sm-special-offer md:w-full flex flex-row justify-start md:justify-center">
							{combo.map((row) => {
								const title = [findProduct(row[0]).product_nm_pub];
								if (row[1]) {
									title.push(` + ${findProduct(row[1]).product_nm_pub}`);
								}

								return (
									<div key={row[0]} className="w-300px md:w-1/5 relative mt-16">
										<h3 className="w-full text-center text-l">{title}</h3>
										<div className="relative">
											<div className="absolute rounded-full w-10 h-10 flex justify-center items-center border-2 border-white bg-green z-30 bottom-4 left-10"><TwoPersonsIcon /></div>
											<img src={productImg({ product_codeid: row[0] }, '2')} alt="" className="relative mx-auto w-190 mt-4 z-10" />
											{row[1] && <img src={productImg({ product_codeid: row[1] }, '2')} alt="" className="absolute w-120 h-120 -bottom-0 right-2 z-20" />}
										</div>
									</div>
								);
							})}
						</div>
					</div>
					<div className="jeej-shelf w-full relative -top-24 md:-top-8" />
				</>

				<div className="flex flex-row justify-center mt-12 flex-wrap">
					<Button color="bg-black md:bg-green" onClick={() => { cartMutate(product, 1, { is_side_dish: false, direction: '+1' }); }}>{t('btn_order')}</Button>
					<button type="button" className="mt-4 md:mt-0 md:ml-6 w-full md:w-auto" onClick={() => { scrollTo('foodMenu'); }}>{t('btn_read_more')}</button>
				</div>
			</div>
		</section>
	);
};

SpecialOffer.propTypes = {
	scrollTo: PropTypes.func.isRequired
};

export default SpecialOffer;
