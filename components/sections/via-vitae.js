import React, { useState } from 'react';

import useLanguage from '../_hooks/useLanguage';
import IconBox from '../partials/icon-box';
import { ActiveLifeIcon, DontBeAngryIcon, EatWellIcon, ReduceIcon } from '../partials/icons';

const ViaVitae = () => {
	const { t } = useLanguage();

	const [activeTab, setActiveTab] = useState(0);
	const [list] = useState([<ActiveLifeIcon />, <EatWellIcon />, 	<ReduceIcon />, <DontBeAngryIcon />]);

	return (
		<section className="md:mt-16 -mb-20 relative z-10">
			<div className="jeej-container text-center relative">
				<div className="bg-via-vitae bg-no-repeat bg-center bg-top bg-cover flex justify-center">
					<div className="absolute bottom-0 left-0 w-full h-1/2 bg-gradient-to-t from-dark" />
					<div className="flex flex-col min-h-via-vitae py-12 md:py-20 relative z-10 w-full">
						<h2 className="text-6xl">{t('label_via_vitae_title')}</h2>
						<p className="text-sm pt-6 px-6 max-w-xl mx-auto">{t('label_via_vitae_description')}</p>
						<div className="w-full p-6 flex justify-start md:justify-center mt-auto mx-auto overflow-scroll md:overflow-auto">
							{list.map((icon, index) => {
								const stringIndex = index + 1;

								return (
									<IconBox
										key={stringIndex}
										title={t(`label_via_viate_item${index + 1}_title`)}
										textColor="white"
										className={`border-solid border-white bg-white bg-opacity-60 w-123 h-123 ${activeTab === index ? 'active' : ''}`}
										onClick={() => setActiveTab(index)}
									>
										{icon}
									</IconBox>
								);
							})}
						</div>

						{list.map((icon, index) => {
							const stringIndex = index + 1;
							return activeTab === index && <p key={stringIndex} className="text-sm max-w-2xl mx-auto px-6 mt-8 text-white">{t(`label_via_viate_item${index + 1}_description`)}</p>;
						})}
					</div>
				</div>
			</div>
		</section>
	);
};

export default ViaVitae;
