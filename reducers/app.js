const reducer = (state, action) => {
	switch (action.type) {
	// == Loader
	case 'SET_LOADER_DATA': {
		const { payload } = action;
		const { name, data } = payload;

		const loadedCnt = state.loadedCnt + 1;
		return { ...state, [name]: data, loadedCnt, loaded: loadedCnt >= 1, [`is${name.charAt(0).toUpperCase() + name.slice(1)}Loaded`]: true };
	}

	// == Side dish
	case 'TOGGLE_SIDE_DISH_SELECTION': {
		const { payload } = action;
		const quantity = (typeof payload !== 'undefined' && payload.quantity) ? payload.quantity : 1;
		return { ...state, selectSideDish: !state.selectSideDish, lastQuantity: !state.selectSideDish ? quantity : 1 };
	}

	// == Contact
	case 'UPDATE_CONTACT': {
		const { payload } = action;
		return { ...state, contact: { ...state.contact, ...payload } };
	}

	// == Modal
	case 'TOGGLE_MODAL': {
		const { payload } = action;
		const { component, data } = payload;
		let d = { ...data };

		if (!component && state.modal.data && state.modal.data.onClose) {
			state.modal.data.onClose(state.modal.data);
			d = {}; // reset data
		}

		return { ...state, modal: { component, data: d } };
	}

	default:
		return state;
	}
};

export default reducer;
