const reducer = (state, action) => {
	switch (action.type) {
	case 'SET_LIST': {
		const { payload } = action;
		return { ...state, list: payload };
	}

	default:
		return state;
	}
};

export default reducer;
