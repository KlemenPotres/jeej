const reducer = (state, action) => {
	switch (action.type) {
	case 'CART_HEAD_UPDATE': {
		const { payload } = action;
		return { ...state, head: payload };
	}

	case 'CART_UPDATE': {
		const { payload } = action;

		return { ...state, list: payload, step: !state.step ? 'CART' : state.step };
	}

	case 'CART_CHANGE_STEP': {
		const { payload } = action;
		return { ...state, step: payload };
	}

	case 'LAST_ADDED_BY_TYPE': {
		const { payload } = action;
		return { ...state, ...payload };
	}

	default:
		return state;
	}
};

export default reducer;
