const request = {
	request: {
		name: 'location_checker'
	},

	form: {
		fieldProps: {
			postal_address: {
				required: true
			},

			postal_postal_code: {
				required: true
			}
		}
	},

	gql: null
};

export default request;
