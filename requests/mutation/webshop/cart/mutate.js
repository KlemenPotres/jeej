import { gql } from 'graphql-request';

const request = {
	request: {
		name: 'webshop>cart>mutate'
	},

	gql: {
		schema: gql`
			mutation CartMutate(
			  $product_codeid: String,
				$quantity: Int
			) {
			  request(
			    product_codeid: $product_codeid
					quantity: $quantity
			  ) {
					product_codeid
					product_nm
					price
					quantity
			  }
			}
		`
	}
};

export default request;
