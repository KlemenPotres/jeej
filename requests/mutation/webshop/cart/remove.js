import { gql } from 'graphql-request';

const request = {
	request: {
		name: 'webshop>cart>remove'
	},

	gql: {
		schema: gql`
			mutation CartRemove(
			  $product_codeid: String
			) {
			  request(
			    product_codeid: $product_codeid
			  ) {
					product_codeid
					product_nm
					price
					quantity
			  }
			}
		`
	}
};

export default request;
