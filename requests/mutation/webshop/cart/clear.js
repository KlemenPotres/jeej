import { gql } from 'graphql-request';

const request = {
	request: {
		name: 'webshop>cart>clear'
	},

	gql: {
		schema: gql`
			mutation CartClear {
			  request {
					status
			  }
			}
		`
	}
};

export default request;
