import { gql } from 'graphql-request';

const request = {
	request: {
		name: 'webshop>checkout'
	},

	form: {
		fieldProps: {
			givenname: {
				required: true
			},
			surname: {
				required: true
			},
			telephone: {
				required: true
			},
			email: {
				required: true,
				pattern: {
					value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
					message: 'Napačna oblika e-mail naslova'
				}
			},
			postal_address: {
				required: true
			},
			postal_postal_code: {
				required: true
			},
			notes: {

			},
			gdpr_agreement: {
				required: true
			}
		}
	},

	gql: {
		schema: gql`
			mutation Checkout(
				$givenname: String,
				$surname: String,
				$telephone: String,
				$email: String,
				$postal_address: String,
				$postal_postal_code: String,
				$notes: String,
				$payment_type_id: String,
				$shipping_id: String
			) {
				request(
					givenname: $givenname,
					surname: $surname,
					telephone: $telephone,
					email: $email,
					postal_address: $postal_address,
					postal_postal_code: $postal_postal_code
					notes: $notes,
					payment_type_id: $payment_type_id,
					shipping_id: $shipping_id
				) {
					id
				}
			}
		`
	}
};

export default request;
