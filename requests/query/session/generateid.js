import { gql } from 'graphql-request';

const request = {
	request: {
		name: 'session>generateid'
	},

	gql: {
		schema: gql`
			query SessionGenerateId {
			  request {
			    possessid
			  }
			}
		`
	}
};

export default request;
