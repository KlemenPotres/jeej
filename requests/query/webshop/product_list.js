import { gql } from 'graphql-request';

const request = {
	request: {
		name: 'webshop>product_list'
	},

	gql: {
		schema: gql`
			query ProductList {
				request {
					product_codeid
					product_category1_id
					product_category2_id
					price
					product_nm_pub
					description
				}
			}
		`
	}
};

export default request;
