import { gql } from 'graphql-request';

const request = {
	request: {
		name: 'webshop>cart>list'
	},

	gql: {
		schema: gql`
			query CartList {
				request {
					product_codeid
			    product_nm
					price
					quantity
				}
			}
		`
	}
};

export default request;
