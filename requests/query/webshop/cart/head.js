import { gql } from 'graphql-request';

const request = {
	request: {
		name: 'webshop>cart>head'
	},

	gql: {
		schema: gql`
			query CartHead {
				request {
					grand_total
					detailed
				}
			}
		`
	}
};

export default request;
