import { gql } from 'graphql-request';

const request = {
	properties: {
		name: 'coderegister>loader'
	},

	data: null,

	gql: {
		schema: gql`
			query CoderegisterLoader(
			  $name: String!
			) {
			  request(
			    name: $name,
			  ) {
			    loader
			  }
			}
		`
	}
};

export default request;
