// tailwind.config.js -- https://tailwindcss.com/docs/configuration
module.exports = {
	purge: ['./pages/**/*.js', './components/**/*.js'],
	darkMode: false, // or 'media' or 'class' -- https://tailwindcss.com/docs/dark-mode
	theme: {
		colors: {
			black: '#000000',
			white: '#FFFFFF',
			green: '#9BBE7B',
			red: '#C80031',
			greenLight: '#DEF6BE',
			lightGreen: '#cadcba',
			orange: '#EA5402',
			yellow: '#ddf21a',
			gray: '#D6D6D6',
			gray2: '#E9E9E9',
			gray3: '#707070',
			gray4: '#474747',
			gray5: '#E3E2E7',
			gray6: '#EBEBEB',
			gray7: '#d2d2d2',
			grayDark: '#2C2C2C',
			grayDark2: '#545454',
			brown: '#36170e',
			dark: '#000000B3',
			error: '#cc6666',
			modal: '#fbfbfb'
		},
		fontFamily: {
			sans: ['Literata', 'sans-serif']
		},
		boxShadow: {
			DEFAULT: '0px 0px 6px #00000029'
		},
		extend: {
			height: theme => ({
				'hero': 'calc(100vh - 220px)',
				'hero-bg': 'calc(100vh - 260px)',
				'150': '150px',
				'123': '123px',
				'57': '57px',
				'47': '47px',
				'35': '35px',
				'250': '250px',
				'600': '600px',
				'100vh': 'var(--vh)'
			}),
			width: theme => ({
				'150': '150px',
				'120': '120px',
				'123': '123px',
				'240': '240px',
				'57': '57px',
				'47': '47px',
				'300%': '300%',
				'35': '35px',
				'190': '190px',
				'250': '250px',
				'300px': '300px',
				'600': '600px',
				'sm-special-offer': '1250px'
			}),
			margin: theme => ({
				'200': '50rem'
			}),
			inset: theme => ({
				'75': '75px'
			}),
			minWidth: theme => ({
				'10': '2.5rem', // same as w-10
				'5': '1.25rem', // same as w-5
				'6': '1.5rem', // same as w-6
				'220': '220px',
				'130': '130px',
				'150': '150px',
				'200px': '200px'
			}),
			minHeight: theme => ({
				'10': '2.5rem', // same as h-10
				'12': '3rem', // same as h-12
				'5': '1.25rem', // same as h-5
				'6': '1.5rem', // same as h-6
				'hero': '823px',
				'hero-bg': '660px',
				'via-vitae': '740px',
				'maja-nina': '728px',
				'maja-nina-sm': '600px',
				'900': '900px',
				'desc': '70px',
				'150': '150px',
				'special-offer': '670px'
			}),
			borderWidth: theme => ({
				'18': '18px',
				'3': '3px'
			}),
			backgroundImage: theme => ({
				'hero': "url('/img/hero-bg-green.jpg')",
				'hero-mobile': "url('/img/hero-mobile-bg.jpg')",
				'footer': "url('/img/footer-bg.svg')",
				'via-vitae': "url('/img/via-vitae-bg.jpg')",
				'nina-maja': "url('/img/jeej-nina-in-maja.jpg')",
				'radio-check': "url('/img/icons/icon-radio-check.svg')",
				'special-offer': "url('/img/special-offer-crate -1.jpg')"
			}),
			backgroundPosition: theme => ({
				'center-center': 'center center',
				'pos-hero-m': '-100px'
			})
		}
	},
	variants: {
		extend: {
			backgroundColor: ['checked'],
			backgroundImage: ['checked'],
			borderColor: ['checked']
		}
	},
	plugins: []
};
