
const lng = {
	upload_error_min_size: 'Datoteka je premajhna.',
	upload_error_max_size: 'Datoteka je prevelika.',
	upload_error_mime_type: 'Napačen format daoteke, dovoljeni formati: %s',
	validation_error_required: 'Obvezno polje.',
	validation_error_password_number: 'Geslo mora vsebovati vsaj eno število!',
	validation_error_password_letter: 'Geslo mora vsebovati vsaj eno črko!',
	validation_error_password_match: 'Gesli se ne ujemata.',
	validation_error_date: 'Napačen format datuma (Pravilen format: %s).',
	validation_error_email: 'Napačen format e-maila.',
	validation_error_numeric: 'Vrednost mora biti numerična.',
	validation_error_max_length: 'Max. število znakov je %s.',
	validation_error_min_length: 'Min. število znakov je %s.',
	validation_error_length: 'Dovoljena dolžina znakov je natanko %s.',
	validation_error_username_exist: 'Uporabniško ime ni na voljo.',
	validation_error_unique: 'Vnos že obstaja.',
	validation_error_email_exist: 'E-mail ne obstaja.',
	validation_recovery_hash_not_valid: 'Unikatna koda za ponastavitev gesla ni veljavna.',
	validation_confirmation_hash_not_valid: 'Potrditvena koda ni veljavna.',
	validation_user_not_found: 'Uporabnik ne obstaja.',
	validation_error_ctype_alpha: 'Dovoljene so samo črke.',
	validation_error_ctype_alnum: 'Dovoljene so samo črke in števila.',
	validation_error_incorrect_password: 'Nepravilno geslo',
	login_error_account_deleted: 'Račun je pobrisan.',
	login_error_account_not_active: 'Račun ni aktiven.',
	login_error_account_not_confirmed: 'Račun ni potrjen.',
	login_error_username_not_found: 'Uporabniški račun še ne obstaja. Potrebna je registracija.',
	login_error_with_old_password: 'Poskušate se prijaviti s starim geslom. Spremeljeno: %s',
	login_error_wrong_password: 'Napačno geslo.',
	login_error_wrong_password_oauth: 'Registrirani ste že s pomočjo: %s (%s). Poskusite se prijaviti s to storitvijo.',
	err_user_not_found: 'Uporabnik ne obstaja.',
	label_tax_number_prefix_taxable: 'ID za DDV:',
	label_tax_number_prefix_nottaxable: 'Davčna št.:',
	label_tax_number_prefix_abroad: 'VAT ID:',
	navItem1: 'Kako deluje?',
	navItem2: 'Lokalno',
	navItem3: 'Filozofija',
	navItem4: 'Kdo kuha?',
	label_hero_title: 'Jej OKej!',
	label_hero_description_line1: 'in pri tem ohrani aktiven življenjski stil. Koncept Jeej! skrbi za uravnoteženo prehrano in okolje.',
	label_hero_description_line2: 'Vsak teden vas pričakujejo nove edinstvene jedi, ki vas bodo z eno dostavo nasitile cel teden.',
	label_special_offer_crate_title: 'Jeej Skupaj!',
	label_special_offer_crate_description: 'Zapelji teden po svoje, oddelaj kar moraš in aktivno porabi svoj čas. <br > Vzami si čas za obrok v družbi. Je bolj okusen! ;)',
	label_process_macro_title: 'Tedensko naročilo domače hrane na dom ali podjetje.',
	label_process_macro_description: 'Izognite se čakalni vrsti dostave in poskrbite, da bo vaš hladilnik poln vnaprej pripravljene sveže hrane primerne tako za dom kot podjetje. Naročila sprejememo do vključno četrtka, na prevzemnem mestu te čaka od ponedeljka naprej. Nekaj planiranja vam ne uide ;)',
	label_process_macro_step1_title: 'IZBERI',
	label_process_macro_step1_description: 'Vsak teden izbiraj med petimi raznolikimi jedmi.',
	label_process_macro_step2_title: 'PREJMI',
	label_process_macro_step2_description: 'Naročila so odprta do četrtka. Na prevzmenm mestu prevzameš v začetku tedna.',
	label_process_macro_step3_title: 'DOKONČAJ',
	label_process_macro_step3_description: 'Vse jedi so pripravljene, da hitro potešijo lakoto. Potrebuješ tudi prilogo? Ni problema.',
	label_process_macro_step4_title: '... PONOVI',
	label_process_macro_step4_description: 'Kuhamo vsak vikend ;)',
	label_process_micro_title: 'Priprava obroka',
	label_process_micro_step1_title: 'SHRANI',
	label_process_micro_step1_description: 'Rok uporabnosti v hladilniku je 5-7 dni',
	label_process_micro_step2_title: 'PRIPRAVI',
	label_process_micro_step2_description: 'Servirajte si jed in se prepustite užitku ...',
	label_process_micro_step3_title: 'OPERI IN VRNI',
	label_process_micro_step3_description: 'Oprano embalažo prevzamemo takrat, ko ti odgovarja.',
	label_process_micro_step4_title: '... PONOVNO NAROČI',
	label_process_micro_step4_description: 'Nov teden, nove jedi.',
	label_banner1_title: 'LOKALNO',
	label_banner1_description: 'Z nakupom lokalnih surovin in izdelkov zmanjšujemo odtis v okolju in podpiramo lokalno skupnost.',
	label_banner2_title: 'IZ DOMAČE KUHINJE',
	label_banner2_description: 'Ker so naše kapacitete omejene, se le hitro postavi v vrsto. Le s kuhanjem omejenih količin lahko zagotovimo kvaliteto, ki jo ponujamo.',
	label_banner3_title: 'Z MISLIJO NA OKOLJE',
	label_banner3_description: 'Jeej! predstavlja skrb za okolje s povratno embalažo in lokalno nabavo.',
	label_features1_title: 'Lokalni dobavitelji',
	label_features1_description: 'Lokalno je boljše!',
	label_features2_title: 'Nabava',
	label_features2_description: 'Več rinfuze, manj embalaže.',
	label_features3_title: 'Kuhanje brez E-jev',
	label_features3_description: 'Edini E-ji v naši hrani so v imenu.',
	label_features4_title: 'Povratna embalaža',
	label_features4_description: 'Menjava polnega kozarca za praznega. Prej boš vrnil prej boš jedel:)',
	label_food_menu_title: '5 okusnih jedi za ta teden',
	label_food_menu_description: 'Dostava ali prevzem vsako nedeljo zvečer ali ponedeljek zjutraj (podjetja).',
	label_food_menu_person_cnt_1: 'Za 1 osebo',
	label_food_menu_person_cnt_2: 'Za 2 osebi',
	label_food_menu_person_cnt_4: 'Za 4 osebe',
	label_food_menu_tab1_title: 'HRANILNE VREDNOSTI',
	label_food_menu_tab2_title: 'SESTAVINE',
	label_food_menu_specs_title: 'Ta obrok zadosti dnevnim vnosom',
	label_food_menu_specs_fat: 'Maščobe',
	label_food_menu_specs_saturate_fat: 'Nasič. maščobe',
	label_food_menu_specs_carbohydrates: 'Oglj. hidrati',
	label_food_menu_specs_fiber: 'Vlaknine',
	label_food_menu_specs_protein: 'Beljakovine',
	label_food_menu_specs_salt: 'Sol',
	label_food_menu_specs_sugar: 'Sladkorji',
	label_food_menu_specs_energy_value: 'Energijska vrednost obroka',
	label_food_menu_specs_quantity: 'Količina',
	label_loading_menu: 'Nalagam menu ...',
	label_select_food_addon: 'Fantastična izbira! Morda potrebujete še prilogo?',
	label_no_side_dish: 'Ne, hvala. Imam.',
	label_main_dish: 'Glavne jedi',
	label_side_dish: 'Priloge',
	label_vegan: 'Vegansko',
	label_gluten_free: 'Brez glutena',
	label_chef_1_title: 'Maja',
	label_chef_1_description: 'Maja vsako jed začini s skrbno izbranimi začimbami in ščepcem dobre volje. Z lokalno izbranimi sestavinami poskrbi, da so jedi kar najbolj hranljive in okusne. Z dolgoletnimi kulinaričnimi ustvarjanji se je izpopolnila v številnih rastlinskih jedeh.',
	label_chef_2_title: 'Nina',
	label_chef_2_description: 'Nina skrbi, da je vsak teden hrana dinamična, polna okusov in z ljubeznijo zložena v kozarce. Skozi leta izkušenj je kuhanje postalo eden njenih ljubših hobijev.',
	label_via_vitae_title: 'Via Vitae',
	label_via_vitae_description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat vel ante a accumsan. In hac habitasse platea dictumst.',
	label_via_viate_item1_title: 'Aktivno življenje',
	label_via_viate_item1_description: 'Vedno najdi čas za sebe in svoje aktivnosti s katerim skrbiš za svoje dobro počutje. Dobra hrana je osnova a še zdaleč to ni vse. Poskrbi za kvalitetno družbo in pravo mero gibanja...',
	label_via_viate_item2_title: 'Jej okej!',
	label_via_viate_item2_description: 'Vedno jej okej in svoje aktivnosti s katerim skrbiš za svoje dobro počutje. Dobra hrana je osnova a še zdaleč to ni vse. Poskrbi za kvalitetno družbo in pravo mero gibanja...',
	label_via_viate_item3_title: 'Zmanjšuj',
	label_via_viate_item3_description: 'Vedno zmanjšaj svoje aktivnosti s katerim skrbiš za svoje dobro počutje. Dobra hrana je osnova a še zdaleč to ni vse. Poskrbi za kvalitetno družbo in pravo mero gibanja...',
	label_via_viate_item4_title: 'Človek, ne jezi se',
	label_via_viate_item4_description: 'Vedno bodi človek, ki se ne jezi in s katerim skrbiš za svoje dobro počutje. Dobra hrana je osnova a še zdaleč to ni vse. Poskrbi za kvalitetno družbo in pravo mero gibanja...',
	label_copyrights: '2021 Zadruga Via Vitae, zadruga za razvoj trajnostnega načina življenja, z.o.o.',
	label_follow_us: 'Sledite nam',
	label_contact: 'Nismo povedali vsega, kar te zanima?',
	label_cart_items_title: 'Naročilo',
	label_cart_items_packaging: 'Embalaža',
	label_cart_delivery_title: 'Dostava',
	label_cart_payment_title: 'Plačilo',
	label_cart_price_items: 'Cena naročila',
	label_cart_price_delivery: 'Dostava',
	label_cart_price_bail: 'Kavcija',
	label_cart_delivery_gratis: 'Brezplačna',
	label_cart_price_grand_total: 'Skupaj',
	label_cart_payment_type1: 'Plačilo po povzetju',
	label_cart_payment_type2: 'Nakazilo na TRR',
	label_cart_shipping_type1: 'Dostava na dom (ned 18 - 20h)',
	label_cart_shipping_type2: 'Osebni prevzem (pon 9 - 17h)',
	label_cart_returning_packaging: 'Vračam kozarce',
	label_cart_returning_box: 'Vračam gajbico',
	label_cart_returning_both: 'Vračam kozarce in gajbico',
	label_cart_disclaimer: '* v kolikor že imate našo embalažo ustrezno označite',
	label_givenname: 'Ime',
	label_surname: 'Priimek',
	label_phone: 'Telefon',
	label_mail: 'E-mail',
	label_address: 'Ulica in hišna številka',
	label_postal_code: 'Pošta',
	label_note: 'Imate morda še kakšna dodatna vprašanja ... ?',
	label_cart_gdrp2_text: 'Prostovoljno in izrecno dovoljujem uporabo svojih osebnih podatkov za potrebe obdelave naročila',
	cookiesConsentText: 'Na spletni strani uporabljamo piškotke, s pomočjo katerih izboljšujemo Vašo uporabniško izkušnjo in zagotavljamo kakovostne vsebine. Z nadaljnjo uporabo se strinjate z uporabo piškotkov.',
	cookiesConsentOkText: 'OK',
	label_order_issued_title: 'Jeeeeej! Naročilo je bilo uspešno oddano.',
	label_order_issued_text: 'Kopijo oddanega naročila ste prejeli na vaš email. <br /> V primeru dodatnih vprašanj smo vam na voljo na <a href="mailto:zdravo@jeej.si">zdravo@jeej.si</a>',
	label_location_checker_title: 'Preverite če dostavljamo do vas.',
	label_location_checker_text: 'Storitev trenutno deluje v <strong>Mariboru in njegovi bližnji okolici (5km okrog centra mesta)</strong>. <br /> V kolikor je vaš naslov izven našega območja dostavljanja nam prosim pišite na <a href="mailto:zdravo@jeej.si">zdravo@jeej.si</a>',
	label_location_checker_valid: 'Jeej! Z veseljem ti dostavimo :)',
	label_location_checker_invalid: 'Oh jooj, predaleč. Lahko morda dostavimo kam bližje?',
	btn_order: 'Naroči',
	btn_add: 'Dodaj',
	btn_recipe: 'Recept',
	btn_tips_and_tricks: 'Tips & tricks',
	btn_order_finish: 'Oddaj naročilo',
	btn_continue: 'Nadaljuj z nakupom',
	btn_creating_order: 'Pripravljam naročilo ...',
	btn_check: 'Preveri',
	btn_close: 'Zapri',
	btn_read_more: 'Preberi več'
};
export default lng;
