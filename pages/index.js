import React from 'react';

import Logo from '../components/partials/logo';

export default function Home() {
	return (
		<center style={{ marginTop: '100px' }}>
			<Logo />
			<div style={{ paddingTop: '15px' }}><strong>Kmalu ;)</strong></div>
		</center>
	);
}
