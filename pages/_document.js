import Document, { Html, Head, Main, NextScript } from 'next/document';

class MyDocument extends Document {
	static async getInitialProps(ctx) {
		const initialProps = await Document.getInitialProps(ctx);
		return { ...initialProps };
	}

	render() {
		return (
			<Html lang="sl">
				<Head>
					<script async src={`https://www.googletagmanager.com/gtag/js?id=${process.env.googleTagId}`} />
					<script
						dangerouslySetInnerHTML={{
						__html: `
								window.dataLayer = window.dataLayer || [];
							  function gtag(){dataLayer.push(arguments);}
							  gtag('js', new Date());
							  gtag('config', '${process.env.googleTagId}');
								`,
						}}
					/>
				</Head>
				<body>
					<Main />
					<NextScript />
				</body>
			</Html>
		);
	}
}

export default MyDocument;
