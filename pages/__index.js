import React, { useState, useRef, useEffect } from 'react';
import CookieConsent from 'react-cookie-consent'; // https://www.npmjs.com/package/react-cookie-consent

import useLanguage from '../components/_hooks/useLanguage';
import useCart from '../components/_hooks/useCart';
import useApp from '../components/_hooks/useApp';
import Head from '../components/_system/head';
import Nav from '../components/nav/nav';
import Header from '../components/header/header';
import Footer from '../components/footer/footer';
import Process from '../components/sections/process';
import SpecialOffer from '../components/sections/special-offer';
// import ViaVitae from '../components/sections/via-vitae'; // <ViaVitae />
import Features from '../components/sections/features';
// import Chefs from '../components/sections/chefs';
import Banner from '../components/partials/banner';
import FoodMenu from '../components/webshop/food-menu/food-menu';
import Cart from '../components/webshop/cart';
import Modal from '../components/modal/modal';
import { ChooseIcon, CompleteIcon, ReceiveIcon, RepeatIcon, CleanAndSaveIcon, StashingIcon, RepeatOrderIcon } from '../components/partials/icons';

export default function Home() {
	const { data: { step } } = useCart();
	const { appData: { modal } } = useApp();

	const { t } = useLanguage();

	const [anchor] = useState({
		foodMenu: useRef(null),
		howItWorks: useRef(null),
		local: useRef(null),
		features: useRef(null),
		chefs: useRef(null)
	});

	const scrollTo = (section) => {
		if (anchor[section]) {
			anchor[section].current.scrollIntoView();
		}
	};

	const handleResize = () => {
		// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
		// Then we set the value in the --vh custom property to the root of the document
		document.documentElement.style.setProperty('--vh', `${(window.innerHeight * 0.01) * 100}px`);
	};

	useEffect(() => {
		if (process.browser) {
			handleResize();
			window.addEventListener('resize', handleResize);
			return () => window.removeEventListener('resize', () => handleResize);
		}

		return false;
	}, []);

	useEffect(() => {
		document.body.style.overflow = (step === 'CHECKOUT' || modal.component) ? 'hidden' : 'unset';
	}, [step, modal.component]);

	// <div ref={anchor.chefs}><Chefs /></div>

	return (
		<>
			<Head />
			<Modal />
			<CookieConsent
				location="none"
				cookieName="cc-accept"
				buttonText={t('cookiesConsentOkText')}
				expires={150}
				disableStyles
				acceptOnScroll
			>
				{t('cookiesConsentText')}
			</CookieConsent>
			<Nav scrollTo={scrollTo} />
			<Header buttonText={t('btn_order')} onClick={() => scrollTo('foodMenu')} />
			<main>
				<div ref={anchor.howItWorks}>
					<Process
						title={t('label_process_macro_title')}
						text={t('label_process_macro_description')}
						name="macro"
						list={[<ChooseIcon />, <ReceiveIcon />, <CompleteIcon />, <RepeatIcon />]}
						buttonText={t('btn_order')}
						onClick={() => scrollTo('foodMenu')}
						styles={{ body: 'pt-0 md:pt-8 pb-16' }}
					/>
				</div>
				<section ref={anchor.local}>
					<div className="md:pb-0 flex flex-wrap">
						{[
							{ id: 1, title: t('label_banner1_title'), description: t('label_banner1_description'), image: '/img/img-1.jpg' },
							{ id: 2, title: t('label_banner2_title'), description: t('label_banner2_description'), image: '/img/img-2.jpg' },
							{ id: 3, title: t('label_banner3_title'), description: t('label_banner3_description'), image: '/img/img-3.jpg' }
						].map(row => <Banner key={row.id} title={row.title} description={row.description} img={row.image} />)}
					</div>
				</section>
				<div ref={anchor.foodMenu}><SpecialOffer scrollTo={scrollTo} /></div>
				<div ref={anchor.foodMenu}><FoodMenu /></div>
				<Process
					name="micro"
					title={t('label_process_micro_title')}
					list={[<StashingIcon />, <CompleteIcon />, <CleanAndSaveIcon />, <RepeatOrderIcon />]}
					styles={{ header: 'pt-16 pb-4', headerTitle: 'text-2xl font-bold text-green', body: 'pb-16' }}
				/>
				<div ref={anchor.features}><Features /></div>
			</main>
			<Footer scrollTo={scrollTo} />
			<Cart />
		</>
	);
}
