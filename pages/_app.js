import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { QueryClient, QueryClientProvider } from 'react-query';
import * as Sentry from '@sentry/react';
import { Integrations } from '@sentry/tracing';

import '../styles/globals.scss';
import LoaderLine from '../components/loader/line';
import Providers from '../contexts/providers';
import AppLoader from '../components/_system/app-loader';
import ErrorBoundary from '../components/_system/error-boundary';

Sentry.init({
	dsn: 'https://f67ec7f4ca2947d5bdb537a300e13977@o528583.ingest.sentry.io/5646028',
	integrations: [new Integrations.BrowserTracing()],

	// We recommend adjusting this value in production, or using tracesSampler
	// for finer control
	tracesSampleRate: 1.0
});

const MyApp = ({ Component, pageProps }) => {
	const [loadedCnt, incLoadedCnt] = useState(0);

	return (
		<ErrorBoundary>
			<QueryClientProvider client={new QueryClient()}>
				<Providers>
					<AppLoader onLoad={() => incLoadedCnt(loadedCnt + 1)} load={{ language: true, coderegister: true }} />
					{ loadedCnt < 1 && <LoaderLine /> }
					<Component {...pageProps} />
				</Providers>
			</QueryClientProvider>
		</ErrorBoundary>
	);
};

MyApp.propTypes = {
	Component: PropTypes.func.isRequired,
	pageProps: PropTypes.shape({}).isRequired
};

export default MyApp;
