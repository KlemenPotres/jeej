import React, { createContext, useContext, useReducer } from 'react';
import PropTypes from 'prop-types';

import reducer from '../reducers/app';

const AppContext = createContext();
export default AppContext;

export function AppProvider({ children }) {
	const [data, dispatch] = useReducer(reducer, {
		// == Loader
		loadedCnt: 0, // when cnt = 3 (config, language, coderegister), then loaded is turned to "true"
		loaded: false,
		language: {},
		isLanguageLoaded: false,
		config: {},
		isConfigLoaded: false,
		coderegister: {},
		isCoderegisterLoaded: false,

		// == Main dish
		lastQuantity: 1, // used, when sidedish is added and user select for 2 person for ex.

		// == Side dish
		selectSideDish: false,

		// == Contact
		contact: (process.browser && window.localStorage.getItem('contact')) ? JSON.parse(window.localStorage.getItem('contact')) : {},

		// == Modal
		modal: {
			component: null,
			data: {}
		}
	});

	return (<AppContext.Provider value={{ data, dispatch }}>{children}</AppContext.Provider>);
}

AppProvider.propTypes = {
	children: PropTypes.node.isRequired
};

export const useStore = () => useContext(AppContext);
