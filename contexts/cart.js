import React, { createContext, useContext, useReducer } from 'react';
import PropTypes from 'prop-types';

import reducer from '../reducers/cart';

const CartContext = createContext();
export default CartContext;

export function CartProvider({ children }) {
	const [data, dispatch] = useReducer(reducer, {
		head: {},
		list: [],
		lastMainDish: null, // last added product_codeid (used in cart carousel, to scroll to item)
		lastSideDish: null, // last added product_codeid (used in cart carousel, to scroll to item)
		step: null // null, SIDE_DISH, CART, CHECKOUT
	});

	return (<CartContext.Provider value={{ data, dispatch }}>{children}</CartContext.Provider>);
}

CartProvider.propTypes = {
	children: PropTypes.node.isRequired
};

export const useStore = () => useContext(CartContext);
