import React, { createContext, useContext, useReducer } from 'react';
import PropTypes from 'prop-types';

import reducer from '../reducers/product';

const ProductContext = createContext();
export default ProductContext;

export function ProductProvider({ children }) {
	const [data, dispatch] = useReducer(reducer, {
		list: []
	});

	return (<ProductContext.Provider value={{ data, dispatch }}>{children}</ProductContext.Provider>);
}

ProductProvider.propTypes = {
	children: PropTypes.node.isRequired
};

export const useStore = () => useContext(ProductContext);
