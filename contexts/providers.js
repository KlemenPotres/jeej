import React from 'react';
import PropTypes from 'prop-types';

import { AppProvider } from './app';
import { CartProvider } from './cart';
import { ProductProvider } from './product';

const Providers = ({ children }) => {
	return (
		<AppProvider>
			<ProductProvider>
				<CartProvider>
					{children}
				</CartProvider>
			</ProductProvider>
		</AppProvider>
	);
};

Providers.propTypes = {
	children: PropTypes.node.isRequired
};

export default Providers;
