export const getWindow = () => {
	return (typeof window !== 'undefined') ? window : { top: { location: null } };
};

export const parseURL = (urlParam) => {
	const win = getWindow();

	if (typeof document !== 'undefined') {
		const url = (typeof urlParam !== 'undefined') ? urlParam : win.top.location;
		const parser = document.createElement('a');
		const searchObject = {};

		// Let the browser do the work
		parser.href = url;

		// Convert query string to object
		const queries = parser.search.replace(/^\?/, '').split('&');

		for (let i = 0; i < queries.length; i += 1) {
			const split = queries[i].split('=');
			const [a, b] = split;
			searchObject[a] = b;
		}

		return {
			protocol: parser.protocol ? parser.protocol : 'http:',
			host: parser.host,
			hostname: parser.hostname,
			port: parser.port,
			pathname: parser.pathname,
			search: parser.search,
			searchObject,
			hash: parser.hash
		};
	}

	return {};
};

export const getBaseURL = (urlParam) => {
	const win = getWindow();
	const data = parseURL((typeof urlParam !== 'undefined') ? urlParam : win.top.location);
	return `${data.protocol}//${data.host}/`;
};

export const getUrlPart = (part, urlParam) => {
	const win = getWindow();
	const data = parseURL((typeof urlParam !== 'undefined') ? urlParam : win.top.location);
	return data[part] || null;
};

export const getSearchObject = (urlParam) => {
	return parseURL((typeof urlParam !== 'undefined') ? urlParam : window.top.location).searchObject;
};

export const getURLSegments = (urlParam) => {
	const win = getWindow();
	const url = (typeof urlParam !== 'undefined') ? urlParam : win.top.location;

	const data = parseURL(url);

	if (data.pathname.length > 1) {
		return data.pathname.replace(/^\/|\/$/g, '').split('/');
	}

	return [];
};

export const getHash = (urlParam) => {
	const win = getWindow();
	const url = (typeof urlParam !== 'undefined') ? urlParam : win.top.location;
	const data = parseURL(url);

	return (data.hash.length > 0) ? data.hash : null;
};

export const getURLSegment = (segmentParam, urlParam) => {
	const segments = getURLSegments(urlParam);

	if (segments.length > 0) {
		let segment = (typeof segmentParam !== 'undefined') ? segmentParam : 1;
		segment -= 1;

		if (typeof segments[segment] !== 'undefined') {
			return segments[segment];
		}
	}

	return null;
};
